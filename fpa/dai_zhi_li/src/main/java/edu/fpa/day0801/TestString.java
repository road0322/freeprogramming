package edu.fpa.day0801;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.zip.DataFormatException;

public class TestString {
    public static void main(String [] args)throws ParseException {
//        String a1=13+"";
//        System.out.println(a1);
        String str="2020-10-10";
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        Date date=df.parse(str);
        Calendar c=new GregorianCalendar();
        c.setTime(date);

        System.out.println("日\t一\t二\t三\t四\t五\t六");

        c.set(Calendar.DAY_OF_MONTH,1);

        for(int i=1;i<=31;i++){
            System.out.println();
        }
    }
}
