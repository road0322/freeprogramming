package edu.fpa.day0801;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestCalendar {
    public static void main(String [] args)throws ParseException {
        String str="2020-10-10";//字符串
        DateFormat c1=new SimpleDateFormat("yyyy-MM-dd");//时间对象
        Date date=c1.parse(str);

        Calendar d1=new GregorianCalendar();//日期对象
        d1.setTime(date);
        System.out.println("日\t一\t二\t三\t四\t五\t六");

        d1.set(Calendar.DAY_OF_MONTH,1);

        for(int i=0;i<d1.get(Calendar.DAY_OF_WEEK)-1;i++){
            System.out.print("\t");
        }


        for(int i=1;i<=31;i++){

            System.out.print(d1.get(Calendar.DAY_OF_MONTH)+"\t");


            if (d1.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY){
                System.out.println();//换行
            }
            d1.add(Calendar.DAY_OF_MONTH,1);

        }
    }
}
