package edu.fpa.day0730;

public class Stringbulder03 {
    public static void main(String [] args){
        //使用stringBuilder
        String str="";
        long num1=Runtime.getRuntime().freeMemory();//系统剩余空间
        long time1=System.currentTimeMillis();//获取系统当前时间
        for (int p=0; p<1000;p++){
            str=str+p;
        }
        long num2=Runtime.getRuntime().freeMemory();//内存
        long time2=System.currentTimeMillis();//当前时间
        System.out.println("String所占内存"+(num1-num2));
        System.out.println("String所用时间"+(time2-time1));

    }
}
