package edu.fpa.day0730;

public class StringBuilder02 {
    public static void main(String[] args){
        StringBuffer sb=new StringBuffer();
        for (int i=0;i<26;i++){
            char temp=(char)('a'+i);
            sb.append(temp);
        }
        System.out.println(sb);
        sb.setCharAt(0,'A');
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
        sb.insert(1,'B');
        System.out.println(sb);

    }
}
