package edu.fpa.day0726;

public class TestStringBuilder {
    public static void main(String [] args) {
        StringBuilder c1 = new StringBuilder();
        for (int i = 0; i < 26; i++) {
            char temp = (char) ('a' + i);
            c1.append(temp);
        }
        System.out.println(c1);
        c1.reverse();//取反
        System.out.println(c1);
    }
}
