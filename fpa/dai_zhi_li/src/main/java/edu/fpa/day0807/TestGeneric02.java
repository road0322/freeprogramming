package edu.fpa.day0807;

public class TestGeneric02 {
    public static void main(String []args){
        Gen<String > gen=new Gen<String>();

        gen.setObj("AAA",0);
        gen.setObj("BBB",1);

        System.out.println(gen.get(1));

    }
}
class Gen<E>{
    Object[] obj=new Object[5];
    public void setObj(E e,int index){
        obj[index]=e;

    }
    public E get(int index){
        return (E)obj[index];

    }
}
