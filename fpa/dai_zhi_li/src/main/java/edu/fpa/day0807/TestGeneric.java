package edu.fpa.day0807;

/**
 * 测试泛型
 */
public class TestGeneric {
    public static  void  main(String [] args){
        Generic generic=new Generic();
        generic.setObjects("AAA",0);
        generic.setObjects(9090,1);

        Integer a=(Integer)generic.get(0);

    }
}
class Generic{
    Object[] objects=new Object[5];
    public void setObjects(Object obj,int index){
        objects[index]=objects;
    }
    public Object get(int index){
        return objects[index];

    }
}
