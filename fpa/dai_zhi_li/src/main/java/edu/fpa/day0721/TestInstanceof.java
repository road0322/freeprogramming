package edu.fpa.day0721;

public class TestInstanceof {
    public static void main(String [] args){
        Person02 p1=new Person02();
        System.out.println(p1 instanceof  Person02);
        System.out.println(p1 instanceof  Student02);
    }
}
class Person02{
    int age;
}
class Student02 extends Person02{
    String name;
}
