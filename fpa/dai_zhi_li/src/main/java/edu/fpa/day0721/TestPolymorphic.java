package edu.fpa.day0721;

public class TestPolymorphic {
    public static void main(String [] args){
        //测试多态
        Animal ani=new Animal();
        AnimalsCry(ani);
        Dog dog=new Dog();
        AnimalsCry(dog);
    }
    static void AnimalsCry(Animal something){
        //多态
        something.shot();
    }
}
//父类和子类的class
class Animal{
    public void shot(){
        System.out.println("叫了一声");
    }
}
class Dog extends Animal{
    public void shot(){
        System.out.println("汪汪汪");
    }
}
class Cat extends Animal{
    public void shot(){
        System.out.println("喵喵喵");
    }
}
