package edu.fpa.day0721;

public class TestExtend {
    public static void main (String [] args){
//        Student stu=new Student();//测试继承
//        stu.age=18;
//        stu.study();

        Student stu2=new Student(18,"dzl","SoftwareEngineering","MaoDa");
    }
}
class Person{
    int age;
    String name;
    String major;
    public void study(){
        System.out.println("i am studying English");
    }
}
class Student extends Person{
    String school;
    public void rest(){
        System.out.println("rest for a minute");
    }
    public Student(int age,String name,String major,String school){
       this.age=age;
       this.name=name;
       this.major=major;
       this.school=school;
    }
}
