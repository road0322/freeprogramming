package edu.fpa.day0806;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TestTryOut {
    public static void main(String [] args){
        FileReader fileReader=null;
        try {
            fileReader=new FileReader("e:/bb.txt");

            char c1=(char)fileReader.read();
            System.out.println(c1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
