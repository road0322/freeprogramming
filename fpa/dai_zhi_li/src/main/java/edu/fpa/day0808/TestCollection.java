package edu.fpa.day0808;

import java.util.ArrayList;
import java.util.Collection;

public class TestCollection {
    public static void main(String [] args){
        Collection<String> c=new ArrayList<String >();
        System.out.println(c.size());
        System.out.println(c.isEmpty());

        c.add("戴志立");
        c.add("谭思雨");

        System.out.println(c);
        System.out.println(c.size());


        System.out.println(c.contains("戴志立"));

        Object[] objects=c.toArray();
        System.out.println(objects);

        c.remove("戴志立");
        System.out.println(c);


    }
}

