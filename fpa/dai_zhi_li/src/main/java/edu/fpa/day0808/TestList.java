package edu.fpa.day0808;

import java.util.ArrayList;
import java.util.List;

public class TestList {
    public static void main(String[] args) {
        List<String > list=new ArrayList<>();

        list.add("AA");
        list.add("BB");
        list.add("CC");
        list.add("DD");
        list.add("EE");

        System.out.println(list);

        list.add(2,"dzl");
        System.out.println(list);


        list.add(3,"tsy");
        System.out.println(list);

        list.remove(2);
        System.out.println(list);

        list.add(2,"dzl");
        System.out.println(list);

        list.add(3,"dzl");
        System.out.println(list);


        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(list.get(2));
        System.out.println(list.indexOf("EE"));
        System.out.println(list.indexOf("PPP"));
    }
}
