package edu.fpa.day0808;

import java.util.ArrayList;
import java.util.Collection;

public class TestCollection02 {
    public static void main(String []args){
        Collection<String> c1=new ArrayList<String >();
        Collection<String> c2=new ArrayList<String >();


        c1.add("AAA");
        c1.add("BBB");

        c2.add("AAA");
        c2.add("BBB");
        c2.add("CCC");

        System.out.println(c1);
        System.out.println(c2);
        System.out.println("=======================================");


//        c1.addAll(c2);
//        System.out.println(c1);


//        c2.removeAll(c1);
//
//        c2.retainAll(c1);
//        System.out.println(c2);

//        System.out.println(c2);

//        c2.containsAll(c1);
        System.out.println(c2.containsAll(c1));
    }
}
