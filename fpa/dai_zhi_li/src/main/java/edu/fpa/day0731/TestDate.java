package edu.fpa.day0731;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestDate {
    public static void main(String [] args){
        Date a1=new Date();
        System.out.println(a1);
        System.out.println(a1.getTime());

        DateFormat b1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        String str=b1.format(new Date());
        System.out.println(str);

        Calendar ca=new GregorianCalendar(2999,10,9,22,10,50);
        int year=ca.get(Calendar.YEAR);
        int month=ca.get(Calendar.MONTH);
        //0是一月
        System.out.println(ca);


        //设置日期相关数据
        Calendar c1=new GregorianCalendar();
        c1.set(Calendar.MONTH,4);

        //日期的计算
        c1.add(Calendar.YEAR,9);

    }
}
