package edu.fpa.day0805;

import java.io.File;

public class TestFile {
    public static void main(String [] args){

        File file=new File("C:\\Users\\DAL\\Desktop\\freeprogramming");

        printFile(file,0);
        
    }
    static void printFile(File file,int level){

        for (int i=1;i<level;i++){
            System.out.print("-");
        }

        System.out.println(file.getName());


        if(file.isDirectory()){
            File[] file1=file.listFiles();
            for (File temp:file1
                 ) {
                printFile(temp,level+1);

            }

        }
    }
}
