package edu.fpa.day0720;

public class RecursiveCalls {
    public static void main(String [] args){
        //主方法
        System.out.println(Factorial(6));
        System.out.println(factorial(6));
    }
    //递归调用（求n的阶乘）
    //递归求阶乘
    public static int factorial(int n){
        if(n==1){
            return 1;
        }else return n*factorial(n-1);
    }
    //循环求阶乘

    public static int Factorial(int n){
        int Multiply=1;
     for (;n>0;n--){
         Multiply*=n;
     }
     return Multiply;
    }
}
