package edu.fpa.day0720;

public class HeavyLoad {
    public static void main(String [] args){
        //方法的重载
        System.out.println(Sum(10,20));

    }
    //参数个数不同
    public static int Sum(int x,int y,int z){
        int sum=x+y+z;
        return sum;
    }
    public static int Sum(int x,int y){
        int sum=x+y;
        return sum;
    }
    //  参数类型不同
    public static double Sum(double x,int y){
        double sum=x+y;
        return sum;
    }
    public static double Sum(double x,double y){
        double sum=x+y;
        return sum;
    }
    //参数顺序不同
    public static  double sum(int x,double y){
        double sum=x+y;
        return sum;
    }
}
