package edu.fpa.day0720;

public class This01 {
    int a;
    int b;
    int c;
    This01(int a,int b){
        this.a=a;
        this.b=b;
    }
    This01(int a,int b,int c){
        this(a,b);
        this.c=c;
    }
    void eat(){
        System.out.println("干饭");
    }
    public static void main(String [] args){
        This01 ps=new This01(10,20);
        ps.eat();
    }
}
