package edu.fpa.day0724;

public class AnonymousInner {//主方法
    public static void main(String [] args){
        Test b1=new Test();
        BB c1=new BB();
        b1.Test(c1);

    }
}
class Test extends BB{
    public void Test(AA produce){

        System.out.println(produce.name()+"有"+produce.price()+"元");
    }
}
class BB implements AA{
    @Override
    public int price() {
        return 500000;
    }

    @Override
    public String name() {
        return "戴志立";
    }
}
interface AA{
    int price();
    String name();
}
