package edu.fpa.day0722;

public class FinalTest {
    public static void main(String [] args){
//        final int age=18;
//        age=19;
        //编译报错
        //因为age被final修饰，不可改变
//        yiwu c1=new yiwu(666);

    }
}
final class school {
    int age;
    String name;
    final public void hero(){
        System.out.println("GIAO哥");
    }
}
////class yiwu extends school{//编译报错，被final修饰的类不能继承
//    int num;
//    //public void hero(){}
//    //编译报错因为hero不能重写
//    public void yiwu(int age,String name,int num){
//        this.age=age;
//        this.name=name;
//        this.num=num;
//    }
//}
