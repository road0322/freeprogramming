package edu.fpa.day0722;

public class Arrays {
    public static void main(String [] args){
        //数组的声明
//        int a[];
//        int []b;
//        String i[];
        //数组的初始化
        int a[]=new int [10];
        int b[];
//        b[]=new int [10];//报错
        String c[]=new String[10];
        //遍历
        for (int i=0;i<a.length;i++){
            a[i]=i;
        }
        for(int m:a){
            System.out.println(m);
        }

    }
}
