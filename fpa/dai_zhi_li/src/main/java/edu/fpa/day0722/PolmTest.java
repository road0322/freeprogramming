package edu.fpa.day0722;

public class PolmTest {
    public static void main(String [] args){
        AnimalsCry c1=new AnimalsCry();//实例化对象
        animalsSay(c1);
        Dog d1=new Dog();
        animalsSay(d1);
        animalsSay(new Cat());
    }
    static void animalsSay(AnimalsCry x){//多态
        x.say();
    }
}
class AnimalsCry{//父类
    public void say(){
        System.out.println("say something");
    }

}
class Dog extends AnimalsCry{//子类
    public void say(){
        System.out.println("wangwangwang");
    }
}
class Cat extends AnimalsCry{//子类
    public void say(){
        System.out.println("miaomiaomiao");
    }
}
