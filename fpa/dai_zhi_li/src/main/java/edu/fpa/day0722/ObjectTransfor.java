package edu.fpa.day0722;

public class ObjectTransfor {
    public static void main(String [] args){
        person a1=new person();
        PersonPlay(a1);
        person c1=new son();//向上强制转型
        PersonPlay(c1);
        son d1=(son) c1;//向下强制转型
        PersonPlay(d1);

    }
    static void PersonPlay(person x){
        x.play();
    }


}
class person{

    public void play(){
        System.out.println("正在玩游戏");
    }
}
class son extends person{

    public void play(){
        System.out.println("在打王者");
    }
}
