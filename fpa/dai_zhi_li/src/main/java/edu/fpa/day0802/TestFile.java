package edu.fpa.day0802;

import java.io.File;

public class TestFile {
    public static void main(String[] args) {
        //指定一个文件
        File f = new File("e:/a.txt");
        //判断文件是否存在
        boolean flag = f.exists();
        //如果存在就删除，不存在就创建
        if (flag) {
            //删除
            System.out.println("删除");
            boolean flag1 = f.delete();
            if (flag1) {
                System.out.println("删除成功");
            } else {
                System.out.println("删除失败");
            }

        }
    }
}