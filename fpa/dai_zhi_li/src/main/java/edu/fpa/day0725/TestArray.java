package edu.fpa.day0725;

public class TestArray {

    public static void main(String [] args){
        String i[]={"0","1","2","3","4","5","6","7","8","9"};
        remove(i,7);

    }
    //删除指定索引元素并将原数组返回
    public static String[] remove(String [] s,int index){
        System.arraycopy(s,index+1,s,index,s.length-index-1);
        s[s.length-1]=null;
        for (String m:s
             ) {
            System.out.println(m);
        }
        return s;
    }
}
