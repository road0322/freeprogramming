package edu.fpa.day0725;

import java.util.Arrays;

public class TestBinarySearch {
    public static void main(String [] args){
        int a[]={432,54,87,18,46,84,564,28,567,99};
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
        System.out.println(Search(a,99));
    }
    public static int Search(int array[],int value){
        int low=0;
        int high=array.length-1;


        while (low<=high){
            int middle=(low+high)/2;
            if (array[middle]==value) {
                return middle;
            }
            if(array[middle]>value){
                high=middle-1;
            }
            if (array[middle]<value){
                low=middle+1;
            }
        }
        return -1;
    }

}
