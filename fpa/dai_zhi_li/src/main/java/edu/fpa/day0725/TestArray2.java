package edu.fpa.day0725;

import java.util.Arrays;

public class TestArray2 {
    public static void main(String [] args){
        Object a[]={"一号",001,"流水线","张全蛋"};
        Object b[]={"一号",002,"流水线","王尼玛"};
        Object c[]={"一号",003,"流水线","吴亦凡"};

        Object i[][]=new Object[3][];
        i[0]=a;
        i[1]=b;
        i[2]=c;
        for (Object[] m:i
             ) {
            System.out.println(Arrays.toString(m));
        }

    }
}
