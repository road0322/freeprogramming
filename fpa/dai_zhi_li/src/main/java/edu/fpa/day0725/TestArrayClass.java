package edu.fpa.day0725;

import java.util.Arrays;

public class TestArrayClass {
     public static void main(String[] args){
         int i[]={0,1,2,3,4,5,6,7,8,9};
         System.out.println(i);//输出地址
         System.out.println(Arrays.toString(i));//输出值

         System.out.println(Arrays.binarySearch(i,8));

     }

}
