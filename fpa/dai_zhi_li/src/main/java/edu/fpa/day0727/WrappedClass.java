package edu.fpa.day0727;

public class WrappedClass {
    public static void main(String [] args){
        //基本数据类型转换为包装类对象
        Integer a=new Integer(10);
        Integer b=Integer.valueOf(20);

        //把包装类对象转换为基本数据类型
        int c=a.intValue();
        double d=b.doubleValue();

        //把字符串转换为包装类对象
        Integer e=new Integer("2222");
        Integer f=Integer.parseInt("9999");

        //把包装类对象转换为字符串
        String str=e.toString();


        System.out.println("Int类型最大的整数="+Integer.MAX_VALUE);
        System.out.println("Int类型最小的整数="+Integer.MIN_VALUE);

        Short o=new Short((short)9);
        short p=o.shortValue();

    }
}
