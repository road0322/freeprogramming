package edu.fpa.day0723;

interface Produce{
    int price();
    String name();
}

 class m{
    public void test(Produce produce){
        System.out.println(produce.name()+"的手机要"+produce.price()+"元");
    }
}

public class AnonymousInner {
    public static void main(String [] args){
        m c1=new m();
        c1.test(new Produce() {
            @Override
            public int price() {
                return 8848;
            }

            @Override
            public String name() {
                return "戴志立";
            }
        });
    }
}
