package edu.fpa.day0723;

import java.awt.im.InputMethodRequests;

public class TestAbstract {
    public static void main(String [] args){//主方法
        son c=new son();
        c.run();
    }
}
abstract class person{//抽象类和抽象方法//抽象方法没有方法体
    abstract public void run();
    public void play(){
        System.out.println("play game");
    }


}
class son extends person{
    @Override
    public void run() {
        System.out.println("running");
    }

}
