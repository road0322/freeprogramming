package edu.fpa.day0723;

public class TestInner {
    public static void main(String [] args){
//        Number.b c1=new Number().new b();//实例化非静态内部类
//        c1.c();
        Mathematic.f t1=new Mathematic.f();//实例化静态内部类
        t1.g();
    }
}
class Number{//非静态内部类
    int num=10;
    class b{
        int num=20;
        public void c(){
            int num=30;
            System.out.println(num);
            System.out.println(this.num);
            System.out.println(Number.this.num);
        }
    }
}
class Mathematic{
    int num=1;
    static class f{
        public void g(){
            System.out.println("666");
        }
    }
}
