package edu.fpa.day0720;

/**
 * 自定义一个HashMap
 * @author xsj
 */
public class demo02 {

    Node[] table; //bucket array 位桶数组
    int size;  //存放键值对的个数

    public demo02(){
        table = new Node[16];
    }
    public void put(Object key,Object value){  //定义新节点对象
        Node newNode = new Node();
        newNode.hash = myHash(key.hashCode(),table.length);
        newNode.key = key;
        newNode.value = value;
        newNode.next = null;

        Node temp = table[newNode.hash];
        if (temp == null){
            table[newNode.hash] = newNode;
        }else {

        }
    }

    public static void main(String[] args) {
        demo02 map = new demo02();
        map.put(101,"aa");
        map.put(102,"bb");
        map.put(103,"cc");
        System.out.println(map);
    }

    public int myHash(int v,int length){
        System.out.println("hash in myHash:" + (v&(length - 1)));
        return v&(length - 1);
    }
}
