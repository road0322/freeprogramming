package edu.fpa.day0720;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试Map的常用方法
 * @author xsj
 */
public class demo01 {
    public static void main(String[] args) {
        Employee employee1 = new Employee(1001,"xxx",1000);
        Employee employee2 = new Employee(1002,"sss",2000);
        Employee employee3 = new Employee(1003,"jjj",3000);
        Employee employee4 = new Employee(1001,"aaa",4000);

        Map<Integer,Employee> map = new HashMap<>();
        map.put(1001,employee1);
        map.put(1002,employee2);
        map.put(1003,employee3);
        map.put(1001,employee4);
        Employee emp = map.get(1001);
        System.out.println(emp.getName());
        System.out.println(map);
    }
}

class Employee{
    private int id;
    private String name;
    private double salary;

    public Employee(int id,String name,double salary){
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}