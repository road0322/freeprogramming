package com.atguigu.java;


/**
 * 测试Thread中的常用方法：
 * 1、start():启动当前线程：调用当前线程的run()方法;
 * 2、run(): 通常需要重写Thread类中的此方法，将创建的线程要执行的操作声明在此方法中;
 * 3、currentThread():静态方法，返回执行当前代码的线程
 * 4、getName():获取当前线程的名字
 * 5、setName():设置当前线程的名字
 * 6、yield():释放当前cpu的执行权
 * 7、join():在线程a中调用线程b的join()，此时线程a就加入阻塞状态，直到线程b完全执行完以后，
 * 线程a才结束阻塞状态。
 * 8、stop():已过时。当执行此方法时，强制结束当前线程。
 * 9、sleep(long millitime):让当前线程"睡眠"指定的millitime毫秒。在指定的millitime毫秒时间内，
 * 当前线程是阻塞状态。
 * 10、isAlive():判断当前线程是否存活
 * @author zfh
 * @create 2021-07-22 18:23
 */
public class MyThreadTest {
    public static void main(String[] args) {
        MyThread4 myThread4 = new MyThread4();
        myThread4.setName("次线程1");
        MyThread1 myThread2=new MyThread1("次线程2");
        Thread.currentThread().setName("主线程");

        myThread2.start();
        myThread4.start();
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+":"+i);

            if(i==50)
              try {
                  myThread2.join();
              }catch (Exception e)
              {
                  e.printStackTrace();
              }
        }
        System.out.println(myThread2.isAlive());
    }
}

class MyThread4 extends Thread
{public MyThread4(){}
    public MyThread4(String name)
{
    super(name);
}
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+":"+i);

            if(i==25) {
               Thread.yield();
            }
        }

    }
}
class MyThread1 extends Thread{
    public MyThread1(){

    }
    public MyThread1(String name)
    {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {

            if(i%2!=0)
            {
            try {
                    if (i == 20)
                        sleep(1000);
               System.out.println(Thread.currentThread().getName()+":"+i);
            }  catch (Exception e){
                e.printStackTrace();
            }
            }
    }
    }
}
