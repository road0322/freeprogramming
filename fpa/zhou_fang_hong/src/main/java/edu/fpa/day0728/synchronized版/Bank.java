package synchronized版;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zfh
 * @create 2021-07-28 17:10
 */
   class Banker{
    private double balance=0;
    private ReentrantLock lock=new ReentrantLock();
public Banker(double balance)
{
    this.balance=balance;
}
//存钱
    public  synchronized void deposit(double amt){
    if(amt>0)
    {
        try{
            Thread.sleep(1000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
        balance+=amt;
        System.out.println(Thread.currentThread().getName()+":"+balance+"存钱成功！");
    }

    }

class Customer extends Thread{
  private Banker b;
  public Customer(Banker b)
  {
      this.b=b;
  }

public void run()
{
    for(int i=0;i<3;i++)
    {
        b.deposit(1000);
    }
}


}
public class Bank {
   public static void main(String []args)
   {
       Banker B=new Banker(0);
       Customer c1=new Customer(B);
       Customer c2=new Customer(B);
       c1.setName("甲客户");
       c2.setName("乙客户");
       c1.start();
       c2.start();
   }

}
