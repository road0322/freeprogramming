package com.atguigu.java;

import javax.xml.stream.events.StartDocument;

/**
 * @author zfh
 * @create 2021-07-24 20:00
 * 创建多线程的方式二： 实现Runnable接口
 * 1、创建一个实现Runnable接口的类
 * 2、实现类去实现Runnable中的抽象方法：run()
 * 3、创建实现类的对象
 * 4、将此对象作为参数传递到Thread类的构造器中，创建Thread类的对象
 * 5、通过Thread类的对象调用start()
 */
//
public abstract class ThreadTest1 {
    public static void main(String[] args) {
        //创建实现类的对象
        T t=new T();
        //将此对象作为参数传递到Thread类的构造器中，创建Thread类的对象
        Thread gd=new Thread(t);
        //通过Thread类的对象调用start():1、启动线程 2、调用当前线程的run()-->调用Runnble类型的target的run()
        /* public void run() {
        if (target != null) {
            target.run();
        }
    }*/
        gd.start();
    }

}
//创建一个实现Runnable接口的类
class  T implements Runnable
{
//实现类去实现Runnable中的抽线方法：run();
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if(i%2==0)
                System.out.println(i);
        }
    }
}
