package com.atguigu.java;

/**
 * @author zfh
 * @create 2021-07-21 18:15
 */
class myThread extends Thread
{
    @Override
    public void run() {
        for (int i = 0; i <100 ; i++) {
            if(i%2==0)
                System.out.println(i);
        }
    }
}
public class ThreadTest {
    public static void main(String[] args) {
        //这是主线程的范围
        myThread m1 = new myThread();
        myThread m2=new myThread();
        //这是次线程
        m1.start();
        //这是另外的一个次线程
        m2.start();
        //又回到就是主线程的范围
        m1.run();
    }
}
