package domain;
import service.Status;
import view.TSUtility;
public class Programmer extends Employee {
    private int memberid;
    private Status status=Status.FREE;
    private Equipment equipment;

    public Programmer() {

    }

    public Programmer(int id, String name, int age, double salary, Equipment equipment) {
        super(id, name, age, salary);
        this.equipment = equipment;
    }

    public int getMemberid() {
        return memberid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
    public String toString ()
    {
        return super.getDetails()+"\t程序员\t"+status+"\t\t\t\t\t" +equipment.getDescription();
    }
    protected String getMemberDetails() {
        return getMemberid() + "/" + getDetails();
    }
    public String getDetailsForTeam() {
        return getMemberDetails() + "\t程序员";
    }
}
