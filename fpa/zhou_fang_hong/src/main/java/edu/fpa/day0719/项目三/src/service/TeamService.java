package service;
import domain.Architect;
import domain.Designer;
import domain.Employee;
import domain.Programmer;
import service.Status;
public class TeamService {
    private static int counter=1;
    private final int MAX_MEMBER=5;
    private Programmer[] team=new Programmer[MAX_MEMBER];
    private int total=0;
    public Programmer[]getTeam(){
        Programmer []team=new Programmer[total];
        for(int i=0;i<total;i++)
        {
            team[i]=this.team[i];
        }
        return team;
    }
    public void addMember(Employee e)throws TeamException
    {
if(total>=MAX_MEMBER)
{
    throw new TeamException("成员已满，添加失败");
}
if(!(e instanceof Programmer))
{
    throw new TeamException("该成员不是开发人员，无法添加！");
}if (isExist (e) )
    {
        throw new TeamException("该成员已在开发团队中！");
    }Programmer p=(Programmer) e;
if("BUSY".equals(p.getStatus().getNAME()))
    throw new TeamException("该成员已是某团队成员！");
else if("VOCATION".equals( p.getStatus().getNAME()))
        {
            throw new TeamException("该成员正在休假，无法添加");
        }
    int numOfArch=0, numOfDes=0,numOfPro=0;
    for(int i=0;i<team.length;i++)
    {
        if(team[i]instanceof Architect)
            numOfArch++;
        else if(team[i] instanceof Designer)
            numOfDes++;
        else if(team[i]instanceof Programmer)
            numOfPro++;}
    if(p instanceof Architect){
    if(numOfArch>=1)
        throw new TeamException("团队中至多只能有一名架构师！");}
    else if(p instanceof Designer){
        if(numOfDes>=2)
            throw new TeamException("团队中至多只能有两名设计师！");}
        else if(p instanceof Programmer){if(numOfPro>=3)
        {
            throw new TeamException("团队中至多只能有三名程序员！");
        }}
    p.setStatus(Status.BUSY);
        p.setMemberid(counter++);
        team[total++]=p;
    }



    public boolean isExist(Employee e)
    {
        for(int i=0;i<total;i++)
        {
            if(team[i].getId()==e.getId())
                return true;
        }
        return false;
    }
    public void removeMember(int memberld)throws TeamException{
        int n=0;
        for(;n<total;n++)
        {
            if(team[n].getMemberid()==memberld)
            {
                team[n].setStatus(Status.FREE);
                break;
            }
        }

        if(n==total)
            throw new TeamException("找不到指定memberId的员工，删除失败");

        for(int i=n+1;i<total;i++)
            team[i-1]=team[i];
        counter--;
        team[--total]=null;
    }

}
