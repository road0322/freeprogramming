package domain;

public class Designer extends Programmer{//设计师
    private double bonus;//奖金
    public Designer()
    {

    }
    public Designer(int id,String name,int age,double salary,Equipment  equipment,double bonus)
    {
        super(id,name,age,salary, equipment);
        this.bonus=bonus;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    public String toString(){
        return super.getDetails()+"\t设计师\t"+getStatus()+"\t"+getBonus()+"\t\t\t"+getEquipment().getDescription();
    }
    public String getDetailsForTeam() {
        return getMemberDetails() + "\t设计师\t" + getBonus();
    }
}
