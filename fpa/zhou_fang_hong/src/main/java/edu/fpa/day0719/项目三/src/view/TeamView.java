package view;
import domain.Employee;
import domain.Programmer;
import service.NameListService;
import service.TeamException;
import service.TeamService;
public class TeamView {
    private NameListService listSvc=new NameListService();
    private TeamService  teamSvc=new TeamService();

    public static void main(String []args) throws TeamException { TeamView o=new TeamView();
        boolean ti=true;
        char shu=0;
        while (ti)
        {  if(shu!='1')
        {
            o.listAllEmployees();
        }
            o.enterMainMenu();
            char tui='Y';
            switch (shu=TSUtility.readMenuSelection())
            {
            case '1' :
                o.getTeam();
                break;
            case '2' :
                o.addMember();
                break;
            case '3' :
               o.deleteMember();
               break;
            case '4':
                System.out.println("确认是否退出(Y/N)：");

                if(tui==TSUtility.readConfirmSelection())
                   ti=false;



        }


        }
    }

    public void enterMainMenu()//主界面显示及控制方法
    {
/*
1-团队列表  2-添加团队成员  3-删除团队成员  4-退出   请选择(1-4)：2

---------------------添加成员---------------------
请输入要添加的员工ID：2
添加成功
按回车键继续...

 */System.out.println("1-团队列表  2-添加团队成员  3-删除团队成员  4-退出   请选择(1-4)：");



    }
    private void listAllEmployees()//以表格形式列出公司所有成员
    {    Employee [] emps =listSvc.getAllEmployees();
        System.out.println("\n-------------------------------开发团队调度软件--------------------------------\n");
if(emps==null||emps.length==0)
{
    System.out.println("没有客户记录!");
}else
{
    System.out.println("ID\t\t姓名\t\t年龄\t\t工资\t\t职位\t\t状态\t\t奖金\t\t股票\t\t领用设备");
}
for(int i=0;i<emps.length;i++)
{
    System.out.println(" "+emps[i]);
}

System.out.println("---------------------------------------------------------------------------------------------------");

    }
    private void getTeam()//显示团队成员列表操作
    {
        System.out.println("\t------------------------------------------------------------------------团队成员列表------------------------------------------------------------");
Programmer []team= teamSvc.getTeam();
if(team==null||team.length==0) {
    System.out.println("开发团队目前没有成员！");
    System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------");
}
else{
    System.out.println("TID/ID\t姓名\t\t年龄\t\t工资\t\t职位\t\t奖金\t\t股票\t");
    for (int i=0;i< team.length;i++)
    {
        System.out.println(team[i].getDetailsForTeam());
    }

}

    }
    private void addMember() throws TeamException//实现添加成员操作
    {   System.out.println("你要添加的人员id为：");
  int id=TSUtility.readInt();
        try{
            Employee emp= listSvc.getEmployee(id);
            teamSvc.addMember(emp);
            TSUtility.readReturn();
            System.out.println("添加成功");
        }catch (TeamException e)
        {e.printStackTrace();
        }

    }
    private void  deleteMember()throws TeamException//实现删除成员操作
    { System.out.println("你要删除的人员id为：");
int memberid=TSUtility.readInt();
System.out.println("确认是否删除？(Y/N)");
char isDelete=TSUtility.readConfirmSelection();
            if(isDelete=='N')
        {
            return;
        }else {try{
            teamSvc.removeMember(memberid);
        } catch (TeamException e)
            {System.out.println("删除失败的原因"+e.getMessage());
            }
            }
        TSUtility.readReturn();

    }

}
