package domain;

public class NoteBook implements Equipment{
    private String model;//机器的型号
    private double price;
    public NoteBook(){

    }
    public NoteBook(String model,double price)
    {
        this.model=model;
        this.price=price;
    }
    public String getDescription()
    {
        return " model "+model+"\t\tprice\t"+price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }
}
