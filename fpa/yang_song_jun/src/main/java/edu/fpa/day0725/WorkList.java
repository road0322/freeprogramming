import java.util.*;
public class WorkList {
    public static void main(String[] args) {
        LinkedList a = new LinkedList();
        ADD(a);
        System.out.print("\n删除链表之前");
        PrintList(a);
        System.out.print("\n删除链表之后");
        a.removeAll(a);
        PrintList(a);
    }
    private static void PrintList(LinkedList a) {
        if (a.size()>0) {
            System.out.println("\n输出元素：");
            for (int i = 0; i < a.size(); i++)
                System.out.print(a.get(i) + "       \t");
        }
        else System.out.println("\n空链表无需输出");
    }
    private static void ADD(LinkedList a) {
        int i;
        int t;
        System.out.println("添加元素：");
        for (i = 0; i < 5; i++) {
            t = (int) (Math.random() * 100);
            System.out.print(t + "       \t");
            a.add(t);
        }
    }
}
