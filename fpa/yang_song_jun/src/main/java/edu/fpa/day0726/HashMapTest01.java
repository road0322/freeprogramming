package src.main.java.edu.fpa.day0726;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashMapTest01 {
    public static void main(String[] args) {
        Map<Integer,String> map=new HashMap<>();
        map.put(1,"zhangsan");
        map.put(3,"wanwu");
        map.put(2,"lisi");
        map.put(4,"zhaoliu");
        map.put(4,"king");
        System.out.println(map.size());       //key重复，value会自动覆盖
        //遍历Map集合
        Set<Map.Entry<Integer,String>> set =map.entrySet();
        for (Map.Entry<Integer,String> entry:set){
            System.out.println(entry.getKey()+"="+entry.getValue());
        }
    }
}
