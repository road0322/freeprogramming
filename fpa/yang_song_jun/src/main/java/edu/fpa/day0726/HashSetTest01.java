package src.main.java.edu.fpa.day0726;

import java.util.HashSet;
import java.util.Set;

public class HashSetTest01 {
    public static void main(String[] args) {
        //hashset的特点，无序不可重复
        Set<String> set=new HashSet<>();
        set.add("set1");
        set.add("set1");
        set.add("set3");
        set.add("set3");
        set.add("set2");
        for (String str:set){
            System.out.println(str);
        }
    }
}
