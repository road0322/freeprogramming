package src.main.java.edu.fpa.day0726;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTest01 {
    public static void main(String[] args) {
        //默认初始化容量为10
        List list1=new ArrayList();
        //指定初始化容量
        List list2=new ArrayList(20);
        System.out.println(list1.size()+","+list2.size());    //size是获取集合中元素的个数而不是获取集合的容量
    }
}
