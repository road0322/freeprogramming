package src.main.java.edu.fpa.day0726;

import java.util.LinkedList;
import java.util.List;

public class LinkedListTest01 {
    public static void main(String[] args) {
        List list1=new LinkedList();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        for (int i=0;i<list1.size();i++){
            System.out.println(list1.get(i));
        }
        System.out.println("===================");
        List list2=new LinkedList();
        list2.add("abc");
        list2.add("def");
        list2.add("ghi");
        for (int i=0;i<list2.size();i++){
            System.out.println(list2.get(i));
        }
        System.out.println("==================");
        list2.remove("abc");
        for (int i=0;i<list2.size();i++){
            System.out.println(list2.get(i));
        }
    }
}
