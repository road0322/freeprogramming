package src.main.java.edu.fpa.day0726;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListTest01 {
    public static void main(String[] args) {
        //默认初始化容量为10
        List l=new ArrayList();
        l.add(1);
        l.add(1);
        l.add(2);
        l.add(3);
        l.add(3);
        l.add(1,"A");
        Iterator i= l.iterator();
        while (i.hasNext()){
            Object o=i.next();
            System.out.println(o);
        }
        System.out.println(l.indexOf(1));
        System.out.println(l.lastIndexOf(3));
        l.remove(1);
        System.out.println(l.size());
        l.set(1,2);                                //修改下标的元素
        System.out.println("-----------------------------------");
        for (int a=0;a<l.size();a++){
            Object obj=l.get(a);
            System.out.println(obj);
        }
    }
}
