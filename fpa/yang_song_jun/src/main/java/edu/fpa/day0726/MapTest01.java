package src.main.java.edu.fpa.day0726;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapTest01 {
    public static void main(String[] args) {
        //创建Map集合对象
        Map<Integer,String> map=new HashMap<>();
        //向Map集合中添加键值对
        map.put(1,"zhangsan");       //1在这里进行了自动装箱
        map.put(2,"lisi");
        map.put(3,"wanwu");
        map.put(4,"zhaoliu");
        //通过key获取value
        String value=map.get(1);
        System.out.println(value);
        //获取键值对的数量
        System.out.println("键值对的数量"+map.size());
        //通过key删除key-value
        map.remove(4);
        System.out.println("键值对的数量"+map.size());
        //判断是否包含某个key
        System.out.println(map.containsKey(1));
        //判断是否包含某个value
        System.out.println(map.containsValue("zhangsan"));
        //清空集合
        //map.clear();
        System.out.println("键值对的数量"+map.size());
        //遍历key，通过key获取value
        Set<Integer> keys= map.keySet();
        /*Iterator<Integer> i= keys.iterator();
        while (i.hasNext()){
            Integer key=i.next();
            System.out.println(key+"="+map.get(key));
        }*/
        for(Integer key:keys){
            System.out.println(key+"="+map.get(key));
        }
    }
}
