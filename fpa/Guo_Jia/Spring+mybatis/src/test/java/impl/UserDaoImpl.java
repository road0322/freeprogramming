package impl;

import Dao.UserDao;
import org.mybatis.spring.SqlSessionTemplate;
import pojo.User;

import java.util.List;

public class UserDaoImpl implements UserDao {
    private SqlSessionTemplate session;

    public void setSession(SqlSessionTemplate session) {
        this.session = session;
    }

    public List<User> getUserList() {
        return session.selectList("pojo.User-Mapper.getUserList");
    }
}
