package Dao;

import pojo.User;

import java.util.List;

public interface UserDao {
    public List<User> getUserList();
}
