package SerializationClass;


import java.io.Serializable;

public class Person implements Serializable {
    private String name;
    private int age;
    transient int p=5;
    public Person(String name, int age,int p) {
        this.name = name;
        this.age = age;
        this.p=p;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", p=" + p +
                '}';
    }
}
