package SerializationClass;

import java.io.*;

public class Test01 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person t=new Person("1",5,9);
        Person t1=new Person("2",6,9);
        FileOutputStream k=new FileOutputStream("t.txt");
        ObjectOutputStream a=new ObjectOutputStream(k);
        a.writeObject(t);
        a.writeObject(t1);
        k.close();
        a.close();
        FileInputStream k1=new FileInputStream("t.txt");
        ObjectInputStream a1=new ObjectInputStream(k1);
        System.out.println(a1.readObject());
        System.out.println(a1.readObject());
        k1.close();
        a1.close();
    }
}
