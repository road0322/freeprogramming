package SingleClass;


class A{

}
//懒汉式
class K{
    private static A a=null;

    public K() {
    }

    public static A getA(){
        if(a==null){
            synchronized (K.class){
                if(a==null){
                    a=new A();
                }
            }
        }
        return a;
    }
}
//饿汉式
class P{
    private static A a=new A();

    public static A getA(){
        return a;
    }
}
public class Test01 {
}
