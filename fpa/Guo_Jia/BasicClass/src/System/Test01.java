package System;

import java.io.IOException;

public class Test01 {
    public static void main(String[] args) throws IOException {
        //输出当前秒数
        System.out.println(System.currentTimeMillis());
        //字符串输入加数字输出
        System.out.println(System.in.read());
        //报错
        System.err.println("换行");
    }
}
