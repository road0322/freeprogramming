package String;


public class test01 {
    public static void main(String[] args) {
        String a=new String("hh");
        String a1="hh";
        //引用类型中只有String类可以用使用+号
        //返回的地址不同了
        String a2=a1+"a1";
        //虽然值一样，但指向的地址却是不同的
        System.out.println(a==a1);//false
        System.out.println(a1==a2);//false
        //StringBuffer
        StringBuffer b=new StringBuffer("hh");
        StringBuffer b1=b;
        System.out.println(b==b1);//true
        b1.append("a");
        System.out.println(b);
        //StringBuilder
        StringBuilder c=new StringBuilder(a.toString());
        System.out.println(c);
        //StringBuffer和StringBuilder的区别和相同
        /*相同点:
        1.是可以修改的String类
        2.都是引用数据类型
        3.存储结构都是字符数组
        4.都用于经常修改的字符串
        不同点:
        1.StringBuffer是安全的，因为公开的方法中都是带有锁的，支持多线程使用
        2.StringBuilder是不安全的，但是很快，不支持多线程使用
        * */

    }
}
