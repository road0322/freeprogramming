package Runtimea;

public class Test0 {
    public static void main(String[] args) {
        //返回与当前Java应用关联的runtime对象。大多数Runtime类的方法是实例方法，所以必须被调用与具体的当前运行时对象
        Runtime a=Runtime.getRuntime();
        //返回Java虚拟机可用的处理器数。
        System.out.println(a.availableProcessors());
    }
}
