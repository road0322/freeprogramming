package MathClass;

public class test01 {
    public static void main(String[] args) {
        //Math类中定义的常量
        System.out.println("PI常量的值:"+Math.PI);
        System.out.println("E常量的值:"+Math.E);
        //最大值，最小值，绝对值
        System.out.println("5和3的最大值:"+Math.max(5,3));
        System.out.println("5和3的最小值:"+Math.min(5,3));
        System.out.println("-55的绝对值:"+Math.abs(-55));
    }
}
