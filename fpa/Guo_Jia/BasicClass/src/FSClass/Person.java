package FSClass;

public class Person {
    private String name="无名氏";
    private int age=100;
    static {
        System.out.println("静态方法执行");
    }
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
        System.out.println("默认方法执行");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
