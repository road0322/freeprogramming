package FSClass;

public class Test01 {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        //方法一
        //获取类对象。并且会先初始化类对象
        Class<?> aClass = Class.forName("FSClass.Person");
        //无参初始化
        System.out.println(aClass.newInstance());
        //方法二
        //获取类对象
        Class<Person> personClass = Person.class;
        System.out.println(personClass.newInstance());
        //方法三
        Person a=new Person();
        Class<?> aClass1 = a.getClass();
        System.out.println(aClass1.newInstance());
        
    }
}
