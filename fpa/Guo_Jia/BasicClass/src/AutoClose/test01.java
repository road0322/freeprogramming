package AutoClose;

public class test01 {
    public static void main(String[] args) {
        //只要实现了AutoCloseable接口就可以实现自动关闭
        try(A a=new A()){
            System.out.println("--main--");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
class A implements AutoCloseable{

    @Override
    public void close() throws Exception {
        System.out.println("--close--");
    }
}
