package BrokerClass;


import com.sun.deploy.net.proxy.ProxyHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Test02 {
    public static void main(String[] args) {

        Helloa hello = new HelloaImp();

        InvocationHandler handler = new ProxyClass(hello);

        Helloa proxyHello = (Helloa) Proxy.newProxyInstance(hello.getClass().getClassLoader(), hello.getClass().getInterfaces(), handler);

        proxyHello.hello();
    }
}

interface Helloa{
    public void hello();
}
class HelloaImp implements Helloa{

    @Override
    public void hello() {
        System.out.println("HelloWorld");
    }
}
class ProxyClass implements InvocationHandler{
    private Object a;
    public ProxyClass(Object a){
        this.a=a;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Before invoke "  + method.getName());
        method.invoke(a, args);
        System.out.println("After invoke " + method.getName());
        return null;

    }
}
