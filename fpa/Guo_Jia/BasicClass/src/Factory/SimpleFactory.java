package Factory;


/*简单工厂和工厂模式*/
//interface Product{
//    public void intro();
//}
//
//class ProductA implements Product{
//
//    @Override
//    public void intro() {
//        System.out.println("商品A");
//    }
//}
//class ProductB implements Product{
//    @Override
//    public void intro() {
//        System.out.println("商品B");
//    }
//}

/*简单工厂*/
/*
class FactoryA{

    public Product getProduct(String name) {
        switch (name){
            case "A":return new ProductA();
            case "B":return new ProductB();
            default:
                return null;
        }

    }

}*/
/*工厂模式*/
//interface Factory{
//    public Product getProduct();
//}
//
//class FactoryA implements Factory{
//
//    @Override
//    public Product getProduct() {
//        return new ProductA();
//    }
//}
//class FactoryB implements Factory{
//
//    @Override
//    public Product getProduct() {
//        return new ProductB();
//    }
//}

//interface Product{
//    public void intro();
//}
//
//class ProductA implements Product{
//
//    @Override
//    public void intro() {
//        System.out.println("商品A");
//    }
//}
//class ProductB implements Product{
//    @Override
//    public void intro() {
//        System.out.println("商品B");
//    }
//}

/*抽象工厂*/
interface Product{
    public void intro();
}
abstract class ProductA implements Product {
    public abstract void intro();
}
abstract class ProductB implements Product {
    public abstract void intro();
}

class ProductAa extends ProductA {

    @Override
    public void intro() {
        System.out.println("商品A");
    }
}
class ProductBb extends ProductB {
    @Override
    public void intro() {
        System.out.println("商品B");
    }
}

interface Factory{
    public Product  getProductB();
    public Product  getProductA();
}
class FactoryA implements Factory{

    @Override
    public Product getProductB() {
        return new ProductAa();
    }

    @Override
    public Product getProductA() {
        return new ProductBb();
    }
}
public class SimpleFactory {
    public static void main(String[] args) {
        /*简单工厂模式:
        * 1.只有一个具体的工厂，加上一个抽象商品类，和商品类的具体类
        * */
        /*Product a=new FactoryA().getProduct("A");
        a.intro();
        Product a1=new FactoryA().getProduct("B");
        a1.intro();*/
        /*工厂模式：
        * 1.工厂抽象化，工厂具体化，分工，具备了不用修改源代码就可扩展的能力
        * */
        /*FactoryA a=new FactoryA();
        a.getProduct().intro();
        FactoryB b=new FactoryB();
        b.getProduct().intro();*/
        /*抽象工厂类
        * 1.在工厂模式的基础上，添加抽象抽象商品的类*/
        Factory a=new FactoryA();
        a.getProductA().intro();
        a.getProductB().intro();
    }
}
