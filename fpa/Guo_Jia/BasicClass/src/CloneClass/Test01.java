package CloneClass;

class A implements Cloneable{
    private String name;
    private int age;
    protected Object clone() throws CloneNotSupportedException {
        return  super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
public class Test01 {
    public static void main(String[] args) throws CloneNotSupportedException {
        A p1=new A();
        p1.setAge(31);
        p1.setName("Peter");

        A p2=(A) p1.clone();
        System.out.println(p1==p2);//false
        p2.setName("Jacky");
        System.out.println("p1="+p1);
        System.out.println("p2="+p2);
        System.out.println(p2.getAge());
    }
}
