package ReflectionClass;

public class Test01 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> aClass = Class.forName("ReflectionClass.Teacher");
        System.out.println(aClass.getPackage());//得到类所在的包
        Class<?>[] interfaces = aClass.getInterfaces();
        for(Class a:interfaces){
            System.out.println(a.getName());
        }
        System.out.println(aClass.getModifiers());
    }
}
