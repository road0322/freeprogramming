package ReflectionClass;

abstract class Person {
public abstract void intro();
}
interface K{

}
class Teacher extends Person implements K{
    private String name;
    private int age;

    public Teacher(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("有参构造");
    }

    public Teacher() {
        System.out.println("无参构造");
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void intro() {
        System.out.println("hh");
    }
}
