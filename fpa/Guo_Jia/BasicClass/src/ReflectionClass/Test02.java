package ReflectionClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Test02 {
    public static void main(String[] args) throws NoSuchMethodException, ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class a=Class.forName("ReflectionClass.Teacher");
        Constructor de = a.getConstructor(String.class,int.class);
        de.newInstance("123",12);
        
    }
}
