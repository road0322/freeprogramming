package CharSequence;

class A implements CharSequence{
    private final char[] value;
    private int size;
    A(){
        this.value= new char[]{};
    }
    A(A a) {
        this.value = a.value;
        for(int i=0;value[i]!='\0';i++){
            size++;
        }
    }

    @Override
    public int length() {
        return size;
    }

    @Override
    public char charAt(int index) {
        if(index<0||index>=size){
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return null;
    }
}
public class test01{
    public static void main(String[] args) {
        //CharSequence表示字符串，String和StringBuffer和StringBuilder都是其子类
        A a=new A();
        System.out.println(a.length());
    }
}
