package HHClass;

import java.io.*;
import java.net.URL;

public class Test02 {

    public static void main(String[] args) {
        try {
            workClassLoader networkClassLoader = new workClassLoader();
            Class clazz = networkClassLoader.loadData();
            System.out.println(clazz.getClassLoader());

        } catch (Exception e) {
            e.printStackTrace();

    }

}}
class workClassLoader extends ClassLoader {

        String Path ="D://Message.class";
    protected Class<?> loadData() throws ClassNotFoundException {
        Class clazz = null;
        byte[] classData = getClassData(Path);
        System.out.println(classData);
        clazz = defineClass("HHClass.Message", classData, 0, classData.length);//name是从src下找
        return clazz;

    }

    private byte[] getClassData(String path) {
        InputStream is = null;
        try {
            byte[] buff = new byte[1024 * 4];
            int len = -1;
            is = new FileInputStream(new File(Path));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = is.read(buff)) != -1) {
                baos.write(buff, 0, len);
            }
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    }



