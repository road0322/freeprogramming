package MySqlClass;

import java.sql.*;

public class test02 {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("java.sql.Driver");
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/Mybatis","root","123456");
        conn.setAutoCommit(false);
        PreparedStatement p=conn.prepareStatement("insert into user(id,name,pwd)values (?,?,?)");
        p.setInt(1,8);
        p.setString(2,"hh");
        p.setString(3,"123");
        System.out.println(p.executeUpdate());
//        conn.rollback();
        conn.commit();
        conn.close();
        p.close();
    }
}
