package MySqlClass;

import java.sql.*;

public class test01 {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("java.sql.Driver");
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/Mybatis","root","123456");
        Statement preparedStatement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        ResultSet ts=preparedStatement.executeQuery("select * from user");
        ts.relative(1);//相对当前的位置移动
        System.out.println(ts.getMetaData().getColumnCount());
        while (ts.next()){
            System.out.println(ts.getInt("id")+","+ts.getString("name")+","+ts.getString("pwd"));
        }
        System.out.println(ts.getRow());
        while (ts.next()){
            System.out.println(ts.getInt("id")+","+ts.getString("name")+","+ts.getString("pwd"));
        }
        ts.close();
        conn.close();
        preparedStatement.close();
    }
}