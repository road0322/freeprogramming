package com.guo.boot.Class;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
public class HH {
//    @Bean("p")
//    public Person p(){
//        return new Person("人",13);
//    }
    @Bean("h")
    public Pet per(){
        return new Pet("小猫",1);
    }
    @ConditionalOnBean(name="h")
    @Bean("k")
    public Pet pp(){
        return new Pet("小狗",1);
    }
}
