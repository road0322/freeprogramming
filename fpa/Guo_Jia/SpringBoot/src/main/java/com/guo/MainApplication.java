package com.guo;

import com.guo.boot.Class.Pet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MainApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);
        String[] beanDefinitionNames = run.getBeanDefinitionNames();
        for(String a: beanDefinitionNames){
            System.out.println(a);
        }
//        Pet tom =run.getBean("h",Pet.class);
//        System.out.println(tom);

//        HH bean = run.getBean(HH.class);
//        System.out.println(bean.per()==bean.per());

        Pet bean =  run.getBean("h",Pet.class);
        System.out.println(bean.getClass().getName());
    }
}
