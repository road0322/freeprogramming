package com.guo.boot;

import com.guo.boot.Class.Person;
import com.guo.boot.Class.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*@Controller
@ResponseBody*/
@RestController
public class HelloController {
    @Autowired
    private  Pet pet;
    @Autowired
    private Person person;

    @ResponseBody
    @RequestMapping("/Person")
    public Person getPerson(){
        return person;
    }
    @ResponseBody
    @RequestMapping("/Pet")
    public Pet h() {
        return pet;
    }
    @ResponseBody
    @RequestMapping("/hello")
    public String handle01(){
        return "Hello,Spring Boot01";
    }

}
