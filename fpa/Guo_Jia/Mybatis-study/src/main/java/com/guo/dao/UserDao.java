package com.guo.dao;

import com.guo.pojo.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface UserDao {
    List<User> getUserList();
    List<User> getUserLista(String name);
    void addUserList(User user);
//    使用注解开发
    @Select("select * from user")
    List<User> getUserListb();
    @Select("select * from user where id = ${id}")
    List<User> getUserListc(@Param("id") int id);
}
