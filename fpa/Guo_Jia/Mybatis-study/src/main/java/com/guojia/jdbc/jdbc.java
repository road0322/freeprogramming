package com.guojia.jdbc;

import java.sql.*;

public class jdbc {
    static final private  String url="jdbc:mysql://localhost/Mybatis";
    static final private String driver="com.mysql.jdbc.Driver";

    static final private String name="root";
    static final private String password="123456";

    public static void main(String[] args){
        Connection con=null;
        Statement state=null;
//        PreparedStatement state=null;可以添加参数
        try {

            //启动驱动
            Class.forName(driver);
            con= DriverManager.getConnection(url,name,password);


//            查询
             state= con.createStatement();
            String sql="select * from user";
            ResultSet rs=state.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("id"));
                System.out.println(rs.getString("name"));
                System.out.println(rs.getString("pwd"));
            }

//            //增加数据
//            String sql="insert into user(id,name,pwd)values(?,?,?)";
//            state=con.prepareStatement(sql);
//            state.setInt(1,4);
//            state.setString(2,"狂神");
//            state.setString(3,"123456");
//            int i = state.executeUpdate();
//            System.out.println(i);

            con.close();
            state.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }
}
