package com.guo.test;

import com.guo.dao.UserDao;
import com.guo.pojo.User;
import com.guo.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class test02 {
    @Test
    public  void pp(){
        SqlSession session= MybatisUtils.getSqlSession();
        UserDao mapper = session.getMapper(UserDao.class);
        List<User> userListb = mapper.getUserListb();
        for(User a:userListb){
            System.out.println(a);
        }
        session.close();

    }
}
