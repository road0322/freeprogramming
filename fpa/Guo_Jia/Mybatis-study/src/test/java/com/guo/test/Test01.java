package com.guo.test;

import com.guo.dao.UserDao;
import com.guo.pojo.User;
import com.guo.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;


import java.util.List;

public class Test01 {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Test01.class);
        logger.info("info: 测试log4j");

        SqlSession session= MybatisUtils.getSqlSession();
        UserDao userDao=session.getMapper(UserDao.class);
        List<User> users=userDao.getUserLista(new String("张"));
        for(User a:users){
            System.out.println(a);
        }
        session.close();
    }
}
