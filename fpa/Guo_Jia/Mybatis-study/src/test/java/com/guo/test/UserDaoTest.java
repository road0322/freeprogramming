package com.guo.test;

import com.guo.pojo.User;
import org.apache.ibatis.session.SqlSession;
import com.guo.utils.*;
import com.guo.dao.*;

import java.util.List;

public class UserDaoTest {
    public static void main(String[] args) {
//        获得SqlSession对象
        SqlSession session = MybatisUtils.getSqlSession();
//        方法一：getMapper
        UserDao userDao = session.getMapper(UserDao.class);
        List<User> userList = userDao.getUserList();
        for(User user:userList){
            System.out.println(user);
        }
//        //增加
//        SqlSession session=MybatisUtils.getSqlSession();
//        UserDao userDao=session.getMapper(UserDao.class);
//        userDao.addUserList(new User(5,"hei","46465"));
//        session.commit();
//
//        //关闭SqlSession
        session.close();
    }
}
