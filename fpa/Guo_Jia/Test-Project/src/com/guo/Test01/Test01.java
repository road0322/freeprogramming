package com.guo.Test01;

public class Test01{
    public static void main(String[] args) {
        T<Object> a=new T<Object>();
        a.getE("hh");
        a.getE(123);
        T b=new T();
        b.getE(123);
        b.getE("123");
    }
}
class T<E> {
    Object[] a=new Object[10];
    int i=0;
    void getE(E c){
        a[i++]=c;
        System.out.println(c.getClass().getName());
    }
}
