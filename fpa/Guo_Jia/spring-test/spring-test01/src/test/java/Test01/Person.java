package Test01;

import org.aspectj.lang.JoinPoint;

public class Person {
    private String name;
    public Person(){
        System.out.println("无参构造");
    }
    public Person(String name){
        System.out.println("有参构造");
        this.name=name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void show(){
        System.out.println("Hello,"+name);
    }

}
