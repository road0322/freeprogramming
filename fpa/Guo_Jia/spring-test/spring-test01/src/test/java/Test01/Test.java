package Test01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
        Person a= (Person) context.getBean("person");
        Person b= (Person) context.getBean("person1");
        b.show();
        a.show();
    }
}
