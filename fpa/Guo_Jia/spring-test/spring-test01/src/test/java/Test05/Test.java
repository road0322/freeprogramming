package Test05;

import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        SqlSession  sqlSession=MybatisUtils.getSqlSessionFactory();
        UserDao userDao=sqlSession.getMapper(UserDao.class);
        List<User> userList=userDao.getUserList();
        for(User user:userList){
            System.out.println(user);
        }
        //关闭SqlSession
        sqlSession.close();
    }
}
