package Test05;

import java.util.List;

interface UserDao {
    List<User> getUserList();
}
