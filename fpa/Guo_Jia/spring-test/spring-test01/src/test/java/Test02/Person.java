package Test02;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Person implements InvocationHandler {
    Object a;

    public Person(Object a) {
        this.a = a;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("方法"+method.getName()+"执行");
        Object invoke = method.invoke(a, args);
        System.out.println("方法"+method.getName()+"结束");
        return invoke;
    }
}
