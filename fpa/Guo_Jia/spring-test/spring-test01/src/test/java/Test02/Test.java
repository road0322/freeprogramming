package Test02;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {
        Teacher k=new Teacher();
        InvocationHandler b=new Person(k);
        a c=(a)Proxy.newProxyInstance(k.getClass().getClassLoader(),k.getClass().getInterfaces(),b);
        c.pp();
    }
}
