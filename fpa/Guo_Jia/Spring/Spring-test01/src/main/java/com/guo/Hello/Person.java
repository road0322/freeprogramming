package com.guo.Hello;

public class Person {
    private Hello na;

    public Hello getNa() {
        return na;
    }

    public void setNa(Hello na) {
        this.na = na;
    }

    @Override
    public String toString() {
        return "Person{" +
                "na=" + na +
                '}';
    }
}
