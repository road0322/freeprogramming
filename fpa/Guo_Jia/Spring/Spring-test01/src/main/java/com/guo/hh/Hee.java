package com.guo.hh;

import javax.annotation.Resource;
import java.lang.reflect.Type;

public class Hee {
//    @Autowired  找到名字相同的bean
//    @Resource(name="he")  name相同名字的bean
    @Resource  //先姓名后类型
    private He he;

    @Override
    public String toString() {
        return "Hee{" +
                "he=" + he +
                '}';
    }

}
