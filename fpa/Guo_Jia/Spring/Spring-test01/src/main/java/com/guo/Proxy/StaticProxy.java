package com.guo.Proxy;

interface Rent{
    public void rent();
}
//房东，目的
class Host implements Rent{
    public void rent(){
        System.out.println("房东出租房子");
    }
}

class XProxy implements Rent{
    private Host host;

    public void setHost(Host host) {
        this.host = host;
    }

    @Override
    public void rent() {
        System.out.println("有中介");
        host.rent();
    }
}

class Client{
    public static void main(String[] args) {
        Host a=new Host();
        XProxy b=new XProxy();
        b.setHost(a);
        b.rent();
    }
}

public class StaticProxy {

}
