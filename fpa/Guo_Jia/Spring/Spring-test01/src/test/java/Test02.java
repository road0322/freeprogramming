
import com.guo.Ha.test1;
import com.guo.hh.Hee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test02 {
    public static void main(String[] args) {
        ApplicationContext c = new ClassPathXmlApplicationContext("beans1.xml");
        Hee hee = (Hee) c.getBean("hee");
        test1 a= (test1) c.getBean("test1");
        System.out.println(hee.toString());
        System.out.println(a.toString());

    }
}
