import com.guo.Test.Dao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test03 {
    public static void main(String[] args) {
        ApplicationContext v = new ClassPathXmlApplicationContext("beans3.xml");
        Dao dao = (Dao) v.getBean("daoImpI");
        dao.add();
    }
}
