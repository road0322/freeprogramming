package edu.fpa.day0723;

import java.util.Map;
import java.util.TreeMap;

public class TestTreeMap {
    public static void main(String[] args) {
        Map<Integer,String> treemap = new TreeMap<>();
        treemap.put(3, "aa");
        treemap.put(107, "xx");
        treemap.put(10, "bb");
        for(Integer key:treemap.keySet()) {
            System.out.println(key+"----"+treemap.get(key));
        }
        Employ1 e1 = new Employ1(107, "zhanglaosan", 5000);
        Employ1 e2 = new Employ1(10, "zhanglulu", 50000);
        Employ1 e3 = new Employ1(1007, "zhangluobo", 7000);
        Map<Employ1,String> em = new TreeMap<>();
        em.put(e1,"老三");
        em.put(e2,"噜噜");
        em.put(e3,"萝卜");
        for(Employ1 emp:em.keySet()) {
            System.out.println(emp+"----"+em.get(emp));
        }
    }
}

class Employ1 implements Comparable<Employ1>{
    int id;
    String name;
    double salary;

    public Employ1(int id, String name, double salary) {
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public int compareTo(Employ1 emp) {
        if(this.salary>emp.salary) {
            return 1;
        }else if(this.salary<emp.salary) {
            return -1;
        }else if(this.id>emp.id){
            return 1;
        }else if(this.id<emp.id){
            return -1;
        }else return 0;
    }
    public String toString() {
        return "id:"+id+"nmae:"+name+"salary:"+salary;
    }
}