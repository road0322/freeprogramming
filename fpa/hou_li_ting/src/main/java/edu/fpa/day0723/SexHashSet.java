package edu.fpa.day0723;

import java.util.HashMap;

public class SexHashSet {
    HashMap<Object, Object> map;
    public static final Object P = new Object();

    public SexHashSet() {
        map = new HashMap<>();
    }
    public void add(Object o) {
        map.put(o,P);
    }
    public int size() {
        return map.size();
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(Object o:map.keySet()) {
            sb.append(o+",");
        }
        sb.setCharAt(sb.length()-1, ']');
        return sb.toString();
    }
    public static void main(String[] args) {
        SexHashSet set = new SexHashSet();
        set.add("aa");
        set.add("bb");
        set.add("cc");
        System.out.println(set);
    }
}