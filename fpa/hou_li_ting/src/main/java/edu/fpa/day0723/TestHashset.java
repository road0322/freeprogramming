package edu.fpa.day0723;

import java.util.HashSet;
import java.util.Set;

public class TestHashset {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("aa");
        set.add("bb");
        set.add("aa");
        System.out.println(set);
        set.remove("aa");
        System.out.println(set);
        Set<String> set1 = new HashSet<>();
        set1.add("dd");
        set1.add("ff");
        System.out.println(set1);
        set.addAll(set1);
        System.out.println(set);
        set.retainAll(set1);
    }
}