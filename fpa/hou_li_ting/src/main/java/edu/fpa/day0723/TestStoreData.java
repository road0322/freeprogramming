package edu.fpa.day0723;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestStoreData {
    public static void main(String[] args) {
        Map<String,Object> row1 = new HashMap<>();
        row1.put("id:",1);
        row1.put("name:","张三");
        row1.put("薪水:",3000);
        Map<String,Object> row2 = new HashMap<>();
        row2.put("id:",2);
        row2.put("name:","李四");
        row2.put("薪水:",4000);
        Map<String,Object> row3 = new HashMap<>();
        row3.put("id:",3);
        row3.put("name:","王五");
        row3.put("薪水:",5000);
        List<Map<String,Object>> list = new ArrayList<>();
        list.add(row1);
        list.add(row2);
        list.add(row3);
        for(Map<String,Object> map:list) {
            Set<String> set = map.keySet();
            for(String k:set) {
                System.out.print(k+map.get(k)+"\t");
            }
            System.out.println();
        }
    }
}