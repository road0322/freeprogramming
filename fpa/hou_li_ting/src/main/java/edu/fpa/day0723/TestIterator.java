package edu.fpa.day0723;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestIterator {

    public static void main(String[] args) {
        testIteratorLink();
        testIteratorSet();
        test1IteratorMap();
        test2IteratorMap();
    }

    public static void testIteratorLink() {
        List<String> list = new ArrayList<>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        for(Iterator<String> iter = list.iterator();iter.hasNext();) {
            String s = iter.next();
            System.out.println(s);
        }
    }
    public static void testIteratorSet() {
        Set<String> set = new HashSet<>();
        set.add("dd");
        set.add("ff");
        set.add("gg");
        for(Iterator<String> iter = set.iterator();iter.hasNext();) {
            String s = iter.next();
            System.out.println(s);
        }
    }
    public static void test1IteratorMap() {
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"dd");
        map.put(2,"ff");
        map.put(3,"gg");
        Set<Entry<Integer,String>> ss = map.entrySet();
        for(Iterator<Entry<Integer,String>> iter = ss.iterator();iter.hasNext();) {
            Entry<Integer,String> s = iter.next();
            System.out.println(s.getKey()+"----"+s.getValue());
        }
    }
    public static void test2IteratorMap() {
        Map<Integer,String> map = new HashMap<>();
        map.put(4,"dd");
        map.put(5,"ff");
        map.put(6,"gg");
        Set<Integer> ss = map.keySet();
        for(Iterator<Integer> iter = ss.iterator();iter.hasNext();) {
            Integer s = iter.next();
            System.out.println(s+"----"+map.get(s));
        }
    }
}
