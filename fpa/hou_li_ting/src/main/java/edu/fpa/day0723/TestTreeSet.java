package edu.fpa.day0723;

import java.util.Set;
import java.util.TreeSet;

public class TestTreeSet {
    public static void main(String[] args) {
        Set<Integer> set = new TreeSet<>();
        set.add(10);
        set.add(107);
        set.add(7);
        for(Integer o:set) {
            System.out.println(o);
        }
        Set<Emp1> set1 = new TreeSet<>();
        set1.add(new Emp1(107,"laosan",5000));
        set1.add(new Emp1(10,"lulu",50000));
        set1.add(new Emp1(7,"luobo",10007));
        for(Emp1 emp:set1) {
            System.out.println(emp);
        }
    }
}

class Emp1 implements Comparable<Emp1>{
    int id;
    String name;
    double salary;
    public Emp1(int id, String name, double salary) {
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public int compareTo(Emp1 emp) {
        if(this.salary>emp.salary) {
            return 1;
        }else if(this.salary<emp.salary) {
            return -1;
        }else if(this.id>emp.id){
            return 1;
        }else if(this.id<emp.id){
            return -1;
        }else return 0;
    }
    @Override
    public String toString() {

        return "id:"+id+"nmae:"+name+"salary:"+salary;
    }

}