package edu.fpa.day0719;

public class SetArrayList02<E>{
    private Object[] elementDate;
    private int size;
    private static final int DEFALT_CAPACITY=10;
    public SetArrayList02() {
        elementDate = new Object[DEFALT_CAPACITY];
    }
    public SetArrayList02(int capacity) {
        elementDate = new Object[capacity];
    }

    public void add(E e) {
        if(size == elementDate.length)
        {
            Object[] NewArray =new Object[elementDate.length+(elementDate.length>>1)];
            System.arraycopy(elementDate,0,NewArray,0,elementDate.length);
            elementDate =  NewArray;
        }
        elementDate[size++] = e;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0;i<size;i++) {
            sb.append(elementDate[i]+",");
        }
        sb.setCharAt(sb.length()-1, ']');
        return sb.toString();
    }
    public static void main(String[] args) {
        SetArrayList02<String> s1 = new SetArrayList02<>(20);
        for(int i=0;i<40;i++){
            s1.add("aa"+i);
        }
        System.out.println(s1);
    }
}