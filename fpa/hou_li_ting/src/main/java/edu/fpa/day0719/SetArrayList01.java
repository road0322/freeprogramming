package edu.fpa.day0719;



public class SetArrayList01<E> {
    private Object[] elementDate;
    private int size;
    private static final int DEFALT_CAPACITY=10;
    public SetArrayList01() {
        elementDate = new Object[DEFALT_CAPACITY];
    }
    public SetArrayList01(int capacity) {
        elementDate = new Object[capacity];

    }
    public void add(E e) {
        elementDate[size++] = e;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0;i<size;i++) {
            sb.append(elementDate[i]+",");
        }
        sb.setCharAt(sb.length()-1, ']');
        return sb.toString();
    }
    public static void main(String[] args) {
        SetArrayList01 s1 = new SetArrayList01(20);
        s1.add("aa");
        s1.add("bb");
        System.out.println(s1);
    }
}
