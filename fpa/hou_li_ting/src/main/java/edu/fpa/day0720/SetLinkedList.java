package edu.fpa.day0720;

public class SetLinkedList{
    Node first;
    Node last;
    int size;

    public void add(Object obj){
        Node node = new Node(obj);
        if(size==0) {
            first = node;
            last = node;
            size++;
        }else {
            last.next = node;
            node.next = null;
            node.previous = last;
            last = node;
            size++;
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node node1  = first;
        do{
            Node node2  = node1;
            sb.append(node1.element+",");
            node1 = node2.next;
        }while(node1!=null);
        sb.setCharAt(sb.length()-1,']');
        return sb.toString();
    }
    public static void main(String[] args){
        SetLinkedList s = new SetLinkedList();
        s.add("a");
        s.add("b");
        s.add("c");
        System.out.println(s);
    }
}
