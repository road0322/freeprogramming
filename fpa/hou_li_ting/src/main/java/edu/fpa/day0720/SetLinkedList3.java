package edu.fpa.day0720;

public class SetLinkedList3{
    Node first;
    Node last;
    int size;

    public void add(Object obj){
        Node node = new Node(obj);
        if(size==0) {
            first = node;
            last = node;
            size++;
        }else {
            last.next = node;
            node.next = null;
            node.previous = last;
            last = node;
            size++;
        }
    }
    public Object get(int index) {
        if(index<0||index>size-1) {
            throw new RuntimeException("索引范围异常："+index);
        }
        Node n = null;
        if(index<(size-1)>>1) {
            n = first;
            for(int i=0;i<index;i++) {
                Node n1 = n.next;
                n=n1;
            }}else {
            n = last;
            for(int i = size-1;i>index;i--) {
                Node n1 = n.previous;
                n= n1;
            }
        }
        return n.element;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node node1  = first;
        do{
            Node node2  = node1;
            sb.append(node1.element+",");
            node1 = node2.next;
        }while(node1!=null);
        sb.setCharAt(sb.length()-1,']');
        return sb.toString();
    }
    public static void main(String[] args){
        SetLinkedList3 s = new SetLinkedList3();
        s.add("a");
        s.add("b");
        s.add("c");
        System.out.println(s);
        System.out.println(s.get(1));
        s.add("d");
        s.add("f");
        System.out.println(s);
        System.out.println(s.get(3));
        System.out.println(s.get(5));
    }
}
