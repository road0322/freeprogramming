package edu.fpa.day0720;

public class Node {
    Node previous;
    Node next;
    Object element;
    public Node(Object obj) {
        element = obj;
    }
}