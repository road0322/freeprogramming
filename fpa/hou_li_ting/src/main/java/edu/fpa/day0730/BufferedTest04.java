package edu.fpa.day0730;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedTest04 {

    public static void main(String[] args) {
        File src = new File("hello.txt");
        BufferedWriter b = null;
        try {
            b = new BufferedWriter(new FileWriter(src));
            b.append("zzzzzzz");
            b.newLine();
            b.append("hhhhhhhhh");
            b.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(b!=null)
                try {
                    b.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

}