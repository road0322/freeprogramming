package edu.fpa.day0730;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class BufferedTest03 {

    public static void main(String[] args) {
        File src = new File("hello.txt");
        BufferedReader reader = null;
        try {
            reader =new BufferedReader(new FileReader(src));
            String line = null;
            try {
                while((line=reader.readLine())!=null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(reader!=null)
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

}