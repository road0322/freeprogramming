package edu.fpa.day0730;

public class DecorateTest01 {
    public static void main(String[] args) {
        Person1 p = new Person1();
        p.say();
        Amplifier am = new Amplifier(p);
        am.say();
    }
}
interface Say{
    void say();
}

class Person1 implements Say{
    private int voice = 10;

    @Override
    public void say() {

        System.out.println("人的声音："+this.getVoice());
    }

    public int getVoice() {
        return voice;
    }

    public void setVoice(int voice) {
        this.voice = voice;
    }

}
class Amplifier implements Say{

    private Person1 p;
    Amplifier(Person1 p){
        this.p = p;
    }
    @Override

    public void say() {
        System.out.println("人的声音："+p.getVoice()*100);
    }

}