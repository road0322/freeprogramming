package edu.fpa.day0730;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

public class ConvertTest02 {
    public static void main(String[] args) {
        try(InputStreamReader ir = new InputStreamReader(new URL("http://www.baidu.com").openStream(),"UTF-8");){
            int temp ;
            while((temp=ir.read())!=-1) {
                System.out.print((char)temp);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        test1();
    }
    public static void test1() {
        try(BufferedReader ir = new BufferedReader(new InputStreamReader(new URL("http://www.baidu.com").openStream(),"UTF-8"));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("hello.txt")))){
            String s = null;
            while((s = ir.readLine())!=null) {
                bw.write(s);
                bw.newLine();
                bw.flush();
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
