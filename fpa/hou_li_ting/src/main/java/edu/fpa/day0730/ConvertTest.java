package edu.fpa.day0730;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class ConvertTest {
    public static void main(String[] args) {

        try (BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
             BufferedWriter w = new BufferedWriter(new OutputStreamWriter(System.out));){
            String s = "";
            while(!s.equals("exit")) {
                s=r.readLine();
                w.write(s);
                w.newLine();
                w.flush();
            }
        }catch(IOException e){
            System.out.println("出现异常");
        }
    }
}
