package edu.fpa.day0730;

public class DecorateTest02 {
    public static void main(String[] args) {
        Drink coffee = new Coffee();
        System.out.println(coffee.info()+"->"+coffee.cost());
        Drink milk = new Milk(coffee);
        System.out.println(milk.info()+"->"+milk.cost());
        Drink suger = new Suger(coffee);
        System.out.println(suger.info()+"->"+suger.cost());
        milk = new Milk(suger);
        System.out.println(milk.info()+"->"+milk.cost());
    }
}

interface Drink{
    double cost();
    String info();
}

class Coffee implements Drink{
    private String name = "原味咖啡";
    @Override
    public double cost() {

        return 10;
    }

    @Override
    public String info() {
        // TODO Auto-generated method stub
        return name;
    }

}
abstract class Decorate implements Drink{

    private Drink d;
    Decorate(Drink d){
        this.d = d;
    }
    @Override
    public double cost() {

        return this.d.cost();
    }

    @Override
    public String info() {

        return this.d.info();
    }

}
class Milk extends Decorate{

    Milk(Drink d) {
        super(d);
    }
    public double cost() {

        return super.cost()*4;
    }

    @Override
    public String info() {

        return super.info()+"加了牛奶";
    }
}
class Suger extends Decorate{

    Suger(Drink d) {
        super(d);
    }
    public double cost() {

        return super.cost()*2;
    }

    @Override
    public String info() {

        return super.info()+"加了糖";
    }
}