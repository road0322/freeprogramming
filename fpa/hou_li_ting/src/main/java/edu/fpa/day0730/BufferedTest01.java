package edu.fpa.day0730;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class BufferedTest01 {
    public static void main(String[] args) {
        File src = new File("hello.txt");
        InputStream i = null;
        try {
            i = new BufferedInputStream(new FileInputStream(src));

            byte[] b = new byte[2];
            int len = -1;
            while((len=i.read(b))!=-1) {
                String s = new String(b,0,len);
                System.out.println(s);
            }
        }catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(i!=null)
                try {
                    i.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

}
