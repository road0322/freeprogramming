package edu.fpa.day0730;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BufferedTest02 {
    public static void main(String[] args) {
        File src = new File("hello.txt");
        OutputStream o = null;
        try {
            o = new BufferedOutputStream(new FileOutputStream(src));
            String s = "zzzzzzzzzzzhhhhhhhhhhhhh";
            byte[] b = s.getBytes();
            o.write(b, 0, b.length);
            o.flush();
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(o!=null)
                try {
                    o.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

        }
    }
}
