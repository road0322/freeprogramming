package edu.fpa.day0730;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedTest05{
    public static void main(String[] args) {
        copy("hello.txt","dest.txt");
    }
    public static void copy(String srcpath,String destpath) {
        BufferedReader r = null;
        BufferedWriter w = null;
        try {
            r =new BufferedReader(new FileReader(srcpath));
            w =new BufferedWriter(new FileWriter(destpath));
            String line = null;
            while((line=r.readLine())!=null) {
                w.write(line);
                w.newLine();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(r!=null)
                try {
                    r.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            if(w!=null)
                try {
                    w.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }


}

