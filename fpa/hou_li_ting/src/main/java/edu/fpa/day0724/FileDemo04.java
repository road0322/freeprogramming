package edu.fpa.day0724;

import java.io.File;

public class FileDemo04 {
    public static void main(String[] args) {
        String path = "D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724/FileDemo01.java";
        File scr = new File(path);
        System.out.println("是否存在："+scr.exists());
        System.out.println("是否是文件："+scr.isFile());
        System.out.println("是否是文件夹："+scr.isDirectory());
        System.out.println("--------------");
        File scr2 = new File("D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724");
        System.out.println("是否存在："+scr2.exists());
        System.out.println("是否是文件："+scr2.isFile());
        System.out.println("是否是文件夹："+scr2.isDirectory());
        File scr3 = new File("XXX");
        if(!scr3.exists()) {
            System.out.println("文件不存在");
        }else if(scr3.isFile()) {
            System.out.println("文件存在");
        }else {
            System.out.println("文件夹存在");
        }
    }
}
