package edu.fpa.day0724;

import java.io.File;

public class FileDemo01 {
    public static void main(String[] args) {
        String path = "D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724/FileDemo01.java";
        File src = new File(path);
        System.out.println(src.length());
        File src1 = new File("D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724/FileDemo01.java");
        System.out.println(src1.length());
        File src2 = new File("D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724","FileDemo01.java");
        System.out.println(src2.length());
        File src3 = new File(new File("D:/zhuanyeruanjian/yichang/src/cn/sxt/day0724"),"FileDemo01.java");
        System.out.println(src3.length());
    }
}