package edu.fpa.day0724;

import java.util.ArrayList;
import java.util.List;

public class TestStoreData2 {
    public static void main(String[] args) {
        User u1 = new User(1,"张三",3000);
        User u2 = new User(2,"李四",7000);
        User u3 = new User(3,"王五",5000);
        List<User> list = new ArrayList<>();
        list.add(u1);
        list.add(u2);
        list.add(u3);
        for(User u:list) {
            System.out.println(u);
        }
    }
}

class User{
    private int id;
    private String name;
    private double salary;

    public User() {

    }

    public User(int id, String name, double salary) {
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {

        return "id:"+this.id+"   name: "+this.name+"   salary:"+salary;
    }
}