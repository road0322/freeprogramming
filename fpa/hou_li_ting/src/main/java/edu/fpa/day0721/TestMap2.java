package edu.fpa.day0721;

import java.util.HashMap;
import java.util.Map;

public class TestMap2 {
    public static void main(String[] args) {
        Map<Integer,Employee> m1 = new HashMap<>();
        Employee e1 = new Employee(1001,"张三",5000);
        Employee e2 = new Employee(1002,"李四",6000);
        Employee e3 = new Employee(1003,"王麻子",7000);
        Employee e4 = new Employee(1001,"xxx",8000);
        m1.put(1001,e1);
        m1.put(1002,e2);
        m1.put(1003,e3);
        System.out.println(m1);
        m1.put(1001,e4);
        System.out.println(m1);
        Employee em = m1.get(1001);
        System.out.println(em.getName());
    }

}
class Employee{
    private int id;
    private String name;
    private double salary;

    public String toString() {
        return "id:"+id+" name:"+name+" salary:"+salary;
    }

    public Employee(int id, String name, double salary) {
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }


}