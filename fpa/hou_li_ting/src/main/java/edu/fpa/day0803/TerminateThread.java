package edu.fpa.day0803;

public class TerminateThread implements Runnable{

    private boolean flag = true;

    public void terminate() {
        this.flag = false;
    }
    public static void main(String[] args) {
        TerminateThread tt = new TerminateThread();
        new Thread(tt).start();
        for(int i=0;i<100;i++) {
            System.out.println("main->"+i);
            if(i==90) {
                tt.terminate();
                System.out.println("thread over");
                break;
            }
        }
    }
    @Override
    public void run() {
        int i = 0;
        while(flag) {
            System.out.println(i++);
        }
    }

}