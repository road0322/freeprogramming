package edu.fpa.day0803;

public class BlockedJoin01 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            for(int i=0;i<100;i++) {
                System.out.println("lambda..............."+i);
            }
        });
        t.start();
        for(int i=0;i<100;i++) {
            System.out.println("main..............."+i);
            if(i==20) {
                t.join();
            }
        }
    }
}