package edu.fpa.day0803;

public class DaemonTest {
    public static void main(String[] args) {
        God g = new God();
        You y = new You();
        Thread t1 = new Thread(g);
        Thread t2 = new Thread(y);
        t1.setDaemon(true);
        t1.start();
        t2.start();
    }
}
class You implements Runnable{

    @Override
    public void run() {
        for(int i=0;i<20;i++)
            System.out.println("youyouyou"+i);
    }

}
class God implements Runnable{

    @Override
    public void run() {
        // TODO Auto-generated method stub
        int i=0;
        while(true) {

            System.out.println("godgodgod"+i++);

        }
    }

}