package edu.fpa.day0803;


public class Priority {

    public static void main(String[] args) {
        MyPriority mp = new MyPriority();
        Thread t1 = new Thread(mp,"a");
        Thread t2 = new Thread(mp,"b");
        Thread t3 = new Thread(mp,"c");
        Thread t4 = new Thread(mp,"d");
        Thread t5 = new Thread(mp,"e");
        t1.setPriority(5);
        t2.setPriority(3);
        t3.setPriority(10);
        t4.setPriority(10);
        t5.setPriority(10);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }

}
class MyPriority implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"->"+Thread.currentThread().getPriority());
    }

}