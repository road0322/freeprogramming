package edu.fpa.day0803;

import java.sql.Date;
import java.text.SimpleDateFormat;


public class BlockedSleep02 {

    public static void main(String[] args) throws InterruptedException {
        Date d = new Date(System.currentTimeMillis());
        long len = d.getTime();
        while(true) {
            System.out.println(new SimpleDateFormat("mm:ss").format(d));
            Thread.sleep(1000);
            d = new Date(d.getTime()-1000);
            if(d.getTime()<len-10*1000) {
                break;
            }
        }
    }


}
