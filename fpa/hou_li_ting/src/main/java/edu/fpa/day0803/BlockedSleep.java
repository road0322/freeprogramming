package edu.fpa.day0803;

public class BlockedSleep {

    public static void main(String[] args) throws InterruptedException {
        int num = 10;
        while(num!=-1) {
            Thread.sleep(1000);
            System.out.println(num--);
        }
    }

}
