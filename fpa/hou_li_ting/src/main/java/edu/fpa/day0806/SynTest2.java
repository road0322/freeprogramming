package edu.fpa.day0806;

public class SynTest2 {
    public static void main(String[] args) {
        Account1  account = new Account1("存款",100);
        Drawing1 You = new Drawing1(account ,70,"我");
        Drawing1 duixiang = new Drawing1(account ,80,"对象");
        You.start();
        duixiang.start();
    }
}
class Account1{
    String name;
    int money;
    public Account1(String name, int money) {
        super();
        this.name = name;
        this.money = money;
    }

}
class Drawing1 extends Thread{

    Account1 account;
    private int drawingMoney;
    private int packetTotal;

    public Drawing1(Account1 account, int drawingMoney,String name) {
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;
    }

    @Override
    public  void run() {
        if(account.money<=0) {
            return;
        }
        synchronized (account) {
            if(account.money<drawingMoney)
                return;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            account.money -= drawingMoney;
            packetTotal += drawingMoney;
            System.out.println(this.getName()+"->"+"账户余额为："+account.money);
            System.out.println(this.getName()+"->"+"口袋余额为："+packetTotal);

        }
    }

}