package edu.fpa.day0806;
public class DeadLock {

    public static void main(String[] args) {
        MakeUp mu1 = new MakeUp(0,"a");
        MakeUp mu2 = new MakeUp(1,"b");
        mu1.start();
        mu2.start();
    }

}
class Lipstick{

}
class Mirror{

}

class MakeUp extends Thread{
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();
    int choice;
    String name;
    public MakeUp(int choice,String name) {
        super(name);
        this.choice = choice;

    }
    public void run() {
        makeup();
    }
    public void makeup() {
        if(choice==0) {
            synchronized (lipstick){
                System.out.println(Thread.currentThread().getName()+"获得口红");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                synchronized (mirror){
                    System.out.println(Thread.currentThread().getName()+"获得镜子");
                }
            }
        }else if(choice==1){
            synchronized (mirror){
                System.out.println(Thread.currentThread().getName()+"获得镜子");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                synchronized (lipstick){
                    System.out.println(Thread.currentThread().getName()+"获得口红");
                }
            }
        }
    }
}