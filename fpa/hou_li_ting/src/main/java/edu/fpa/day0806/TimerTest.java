package edu.fpa.day0806;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {

    public static void main(String[] args) {
        Timer t = new Timer();
        t.schedule(new MyTime(), 1000);
    }

}
class MyTime extends TimerTask{

    @Override
    public void run() {
        for(int i=0;i<10;i++)
            System.out.println("aaaaaaa"+i);
        System.out.println("--------------------------------");
    }

}