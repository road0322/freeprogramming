package edu.fpa.day0806;


public class UnsafeTest {
    public static void main(String[] args) {
        Account  account = new Account("存款",100);
        Drawing You = new Drawing(account ,70,"我");
        Drawing duixiang = new Drawing(account ,80,"对象");
        You.start();
        duixiang.start();
    }
}
class Account{
    String name;
    int money;
    public Account(String name, int money) {
        super();
        this.name = name;
        this.money = money;
    }

}
class Drawing extends Thread{

    Account account;
    private int drawingMoney;
    private int packetTotal;

    public Drawing(Account account, int drawingMoney,String name) {
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;
    }

    @Override
    public void run() {
        if(account.money<drawingMoney)
            return;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        account.money -= drawingMoney;
        packetTotal += drawingMoney;
        System.out.println(this.getName()+"->"+"账户余额为："+account.money);
        System.out.println(this.getName()+"->"+"口袋余额为："+packetTotal);

    }

}