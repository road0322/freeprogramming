package edu.fpa.day0725;

import java.io.UnsupportedEncodingException;

public class ContentDecode {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String s = "算法数据结构a";
        byte[] b = s.getBytes();
        System.out.println(b.length);
        b = s.getBytes("UTF-16LE");
        System.out.println(b.length);
        b = s.getBytes("GBK");
        System.out.println(b.length);
        b = s.getBytes();
        //解码
        String mag = new String(b);
        System.out.println(mag);
        //乱码1字节数不够
        mag = new String(b,0,b.length-2,"utf8");
        System.out.println(mag);
        //乱码2字符集不统一
        mag = new String(b,0,b.length-1,"GBK");
        System.out.println(mag);
    }
}
