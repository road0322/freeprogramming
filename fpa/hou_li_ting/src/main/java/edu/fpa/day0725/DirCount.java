package edu.fpa.day0725;


import java.io.File;

public class DirCount {

    private long len;
    private String path;
    private File src;
    private int Filesize;
    private int Dirsize;

    public int getFilesize() {
        return Filesize;
    }
    public void setFilesize(int filesize) {
        Filesize = filesize;
    }
    public int getDirsize() {
        return Dirsize;
    }
    public void setDirsize(int dirsize) {
        Dirsize = dirsize;
    }
    public DirCount(String path) {
        this.path = path;
        this.src = new File(this.path);
        count(this.src);
    }
    private void count(File f) {
        if(f!=null && f.exists()) {
            if(f.isFile()) {
                this.len+=f.length();
                this.Filesize++;
            }else {
                for(File ff:f.listFiles()) {
                    this.Dirsize++;
                    count(ff);
                }
            }
        }
    }

    public long getLen() {
        return len;
    }
    public void setLen(long len) {
        this.len = len;
    }
    public static void main(String[] args) {
        DirCount src = new DirCount("D:/zhuanyeruanjian/yichang/src/cn/sxt");
        System.out.println(src.len+"  文件数："+src.getFilesize()+"  文件夹数："+src.getDirsize());
    }

}