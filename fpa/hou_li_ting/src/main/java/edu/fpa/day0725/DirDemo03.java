package edu.fpa.day0725;

import java.io.File;

public class DirDemo03 {
    public static void main(String[] args) {
        File dir = new File("D:/zhuanyeruanjian/yichang/src/cn/sxt");
        printName(dir,0);
    }
    public static void printName(File f,int d) {
        for(int i=0;i<d;i++) {
            System.out.print("-");
        }
        System.out.println(f.getName());
        if(f==null|| !f.exists()) {
            return ;
        }else if(f.isDirectory()) {
            for(File ff:f.listFiles())
                printName(ff,d+1);
        }
    }
}
