package edu.fpa.day0725;

import java.io.File;

public class DirDemo02 {
    public static void main(String[] args) {
        File dir = new File("D:/zhuanyeruanjian/yichang/src/cn/sxt");
        String[] s = dir.list();
        for(String ss:s) {
            System.out.println(ss);
        }
        File[] ff = dir.listFiles();
        for(File f:ff) {
            System.out.println(f.getAbsolutePath());
        }
        File[] roots = dir.listRoots();
        for(File f:roots) {
            System.out.println(f.getAbsolutePath());
        }

    }
}
