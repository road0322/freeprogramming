package edu.fpa.day0802;

public class lambdaTest03 {
    static class love2 implements Ilove1{

        @Override
        public int lambda(int a,int c) {
            System.out.println("i like lambda,2"+(a+c));
            return (a+c);
        }

    }
    public static void main(String[] args) {
        Ilove1 l = new love1();
        l.lambda(100,1);
        l = new love2();
        l.lambda(100,2);
        class love3 implements Ilove1{

            @Override
            public int lambda(int a,int c) {
                System.out.println("i like lambda,3"+(a+c));
                return (a+c);
            }

        }
        l = new love3();
        l.lambda(100,3);
        l = new Ilove1() {
            @Override
            public int lambda(int a,int c) {
                System.out.println("i like lambda,4"+(a+c));
                return (a+c);
            }
        };
        l.lambda(100,4);
        l = (a,c)->{
            System.out.println("i like lambda,5"+(a+c));
            return (a+c);
        };
        l.lambda(100,5);
        l = (a,c)->(a+c);
        int i = l.lambda(100, 8);
        System.out.println(i);

    }
}
interface Ilove1{
    int lambda(int a,int b);
}

class love1 implements Ilove1{

    @Override
    public int lambda(int a,int c) {
        System.out.println("i like lambda"+(a+c));
        return (a+c);
    }

}