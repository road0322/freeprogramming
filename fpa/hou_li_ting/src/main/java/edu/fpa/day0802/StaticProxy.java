package edu.fpa.day0802;


public class StaticProxy {
    public static void main(String[] args) {
        new Proxy(new You()).happyMarry();
    }
}
interface Marry{
    void happyMarry();
}
class You implements Marry{

    @Override
    public void happyMarry() {
        System.out.println("get married");
    }
}
class Proxy implements Marry{
    private You y;

    public Proxy(You y) {
        this.y = y;
    }
    @Override
    public void happyMarry() {
        begin();
        this.y.happyMarry();
        after();
    }
    private void after() {
        System.out.println("......");
    }
    private void begin() {
        System.out.println("布置");

    }

}