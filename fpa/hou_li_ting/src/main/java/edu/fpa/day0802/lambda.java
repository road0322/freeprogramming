package edu.fpa.day0802;

public class lambda {
    static class test01 implements Runnable{
        @Override
        public void run() {
            for(int i=0;i<20;i++)
                System.out.println("aaaaaaaaaaa");
        }
    }

    public static void main(String[] args) {
        lambda l = new lambda();
        new Thread(new test01()).start();
        for(int i=0;i<20;i++)
            System.out.println("bbbbbbbbbb");
        System.out.println("-------------------------");
        System.out.println("-------------------------");
        System.out.println("-------------------------");

        class test02 implements Runnable{
            @Override
            public void run() {
                for(int i=0;i<20;i++)
                    System.out.println("cccccccccccccccccc");
            }
        }
        new Thread(new test02()).start();
        for(int i=0;i<20;i++)
            System.out.println("ddddddddddddddd");

        new Thread(new Runnable() {
            public void run() {
                for(int i=0;i<20;i++)
                    System.out.println("fffffffffffff");
            }
        }).start();
        new Thread(()-> {
            for(int i=0;i<20;i++)
                System.out.println("fffffffffffff");
        }
        ).start();
    }
}
