package edu.fpa.day0802;


public class lambdaTest02 {
    static class like2 implements Ilove{

        @Override
        public void lambda(int a) {
            System.out.println("i like lambda,2"+a);
        }

    }
    public static void main(String[] args) {
        Ilove l = new love();
        l.lambda(100);
        l = new like2();
        l.lambda(100);
        class like3 implements Ilove{

            @Override
            public void lambda(int a) {
                System.out.println("i like lambda,3"+a);
            }

        }
        l = new like3();
        l.lambda(100);
        l = new Ilove() {
            @Override
            public void lambda(int a) {
                System.out.println("i like lambda,4"+a);
            }
        };
        l.lambda(100);
        l = a-> {
            System.out.println("i like lambda,5"+a);
        };
        l.lambda(100);
        l = a->System.out.println("i like lambda,5"+a);
        l.lambda(100);
    }
}
interface Ilove{
    void lambda(int a);
}

class love implements Ilove{

    @Override
    public void lambda(int a) {
        System.out.println("i like lambda"+a);
    }

}