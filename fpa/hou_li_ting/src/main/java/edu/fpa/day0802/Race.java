package edu.fpa.day0802;
public class Race implements Runnable{

    @Override
    public void run() {
        for(int step=1;step<=100;step++) {
            System.out.println(Thread.currentThread().getName()+": "+step);
            gameOver(step);
        }
    }
    public static void gameOver(int step) {
        if(step==100) {
            System.out.println("winner : "+Thread.currentThread().getName());

        }
    }
    public static void main(String[] args) {
        Race r = new Race();
        new Thread(r,"兔子").start();
        new Thread(r,"乌龟").start();
    }
}
