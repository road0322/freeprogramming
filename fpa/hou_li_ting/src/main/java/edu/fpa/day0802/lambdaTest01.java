package edu.fpa.day0802;


public class lambdaTest01 {
    static class like2 implements Ilike{

        @Override
        public void lambda() {
            System.out.println("i like lambda,2");
        }

    }
    public static void main(String[] args) {
        Ilike l = new like();
        l.lambda();
        l = new like2();
        l.lambda();
        class like3 implements Ilike{

            @Override
            public void lambda() {
                System.out.println("i like lambda,3");
            }

        }
        l = new like3();
        l.lambda();
        l = new Ilike() {
            @Override
            public void lambda() {
                System.out.println("i like lambda,4");
            }
        };
        l.lambda();
        l = ()-> {
            System.out.println("i like lambda,5");
        };
    }
}
interface Ilike{
    void lambda();
}

class like implements Ilike{

    @Override
    public void lambda() {
        System.out.println("i like lambda");
    }

}