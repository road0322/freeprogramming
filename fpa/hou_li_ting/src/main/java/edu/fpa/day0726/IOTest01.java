package edu.fpa.day0726;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

//1.创建流

public class IOTest01 {

    public static void main(String[] args) throws IOException {
        File src = new File("hello.txt");
        try {
            InputStream i = new FileInputStream(src);
            int data1 = i.read();
            int data2 = i.read();
            int data3 = i.read();
            int data4 = i.read();
            System.out.println((char)data1);
            System.out.println((char)data2);
            System.out.println((char)data3);
            System.out.println(data4);
            i.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
    }


}
