package edu.fpa.day0726;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class IOTest03 {

    public static void main(String[] args) {
        File src = new File("hello.txt");
        OutputStream dest = null;
        try {
            dest = new FileOutputStream(src);
            String s= "IO is so easy";
            byte[] b = s.getBytes();
            dest.write(b, 0, b.length);
            dest.flush();

        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(src!=null)
                try {
                    dest.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

}
