package edu.fpa.day0726;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

//1.创建流

public class IOTest02 {

    public static void main(String[] args) throws IOException  {
        File src = new File("hello.txt");
        InputStream i = null;
        try {
            i = new FileInputStream(src);
            int temp = i.read();
            while(temp!=-1) {
                System.out.println((char)temp);
                temp = i.read();
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        finally {
            try {
                if(i!=null)
                    i.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }


}