package edu.fpa.day0726;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

//1.创建流

public class IOTest04 {

    public static void main(String[] args) throws IOException  {
        InputStream i = null;
        try{
            File src = new File("hello.txt");
            i = new FileInputStream(src);
            byte[] flush = new byte[3];
            int len = i.read(flush);
            while(len!=-1) {
                String s = new String(flush,0,len);
                System.out.println(s);
                len = i.read(flush);
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }	finally {
            if(i!=null)
                i.close();
        }

    }

}