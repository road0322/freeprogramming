package edu.fpa.day0726;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyFile {
    public static void main(String[] args) {
        File src1 = new File("dest.txt");
        File src2 = new File("hello.txt");
        InputStream i = null;
        OutputStream o = null;
        try {
            i= new FileInputStream(src1);
            o = new FileOutputStream(src2);
            byte[] b = new byte[30];
            int len = -1;
            while((len= i.read(b))!=-1) {
                o.write(b,0,len);
                String s = new String(b);
                System.out.println(s);
            }
            o.flush();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(src1!=null)
                    i.close();
                if(src2!=null)
                    o.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
