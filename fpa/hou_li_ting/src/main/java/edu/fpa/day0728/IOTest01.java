package edu.fpa.day0728;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class IOTest01 {

    public static void main(String[] args) throws FileNotFoundException {
        File src = new File("hello.txt");
        Writer w = null;
        try {
            w =new FileWriter(src);
            String s = "zhanglaosan";
            char[] c = s.toCharArray();
            w.write(c,0,c.length);
            w.write("add");
            w.append("add1").append("add2");
            w.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(w!=null)
                try {
                    w.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

}