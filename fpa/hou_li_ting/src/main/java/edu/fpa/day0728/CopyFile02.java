package edu.fpa.day0728;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

public class CopyFile02 {
    public static void main(String[] args) throws FileNotFoundException {
        File src1 = new File("hello.txt");
        File src2 = new File("dest.txt");
        Reader r = null;
        Writer w = null;
        try {
            r = new FileReader(src1);
            w = new FileWriter(src2);
            char[] b = new char[2];
            int len = -1;
            while((len=r.read(b))!=-1) {
                w.write(b, 0, b.length);
                w.flush();

            }
        } catch (IOException e) {

            e.printStackTrace();
        }finally {
            if(r!=null)
                try {
                    r.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            if(w!=null)
                try {
                    w.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }
}