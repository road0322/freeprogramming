package edu.fpa.day0728;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOTest05 {
    public static void main(String[] args) {
        File src1 = new File("hello.txt");
        File src2 = new File("dest.txt");
        byte[] b1 = null;
        InputStream i = null;
        OutputStream o = null;
        try {
            i = new FileInputStream(src1);
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            byte[] b2 = new byte[2];
            int len = -1;
            while((len=i.read(b2))!=-1) {
                bo.write(b2,0,b2.length);
            }
            b1 = bo.toByteArray();
            ByteArrayInputStream bi = new ByteArrayInputStream(b1);
            o = new FileOutputStream(src2);
            byte[] b3 =new byte[2];
            int len1 = -1;
            while((len1 = bi.read(b3))!=-1) {
                o.write(b3, 0, len);
            }
            o.flush();
        }catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }finally {
            if(i!=null)
                try {
                    i.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            if(o!=null)
                try {
                    o.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
}