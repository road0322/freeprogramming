package edu.fpa.day0728;
import java.io.ByteArrayOutputStream;

public class IOTest04 {
    public static void main(String[] args) {
        byte[] b1 = null;
        ByteArrayOutputStream o = new ByteArrayOutputStream();
        byte[] b2 = "talk is cheap show me the code".getBytes();
        o.write(b2, 0, b2.length);
        b1 = o.toByteArray();
        System.out.println(new String(b1));
    }
}