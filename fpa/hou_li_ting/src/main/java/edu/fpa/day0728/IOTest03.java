package edu.fpa.day0728;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class IOTest03 {
    public static void main(String[] args) {
        byte[] b = "talk is cheap show me the code".getBytes();
        InputStream i = new ByteArrayInputStream(b);
        byte[] b1 = new byte[2];
        int len = -1;
        try {
            while((len=i.read(b1))!=-1) {
                String s= new String(b1);
                System.out.println(s);

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(i!=null)
                try {
                    i.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }
}