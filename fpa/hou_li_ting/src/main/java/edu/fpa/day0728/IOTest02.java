package edu.fpa.day0728;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class IOTest02 {

    public static void main(String[] args) {
        File src = new File("hello.txt");
        Reader reader = null;
        try {
            reader =new FileReader(src);
            char[] c = new char[2];
            int len = -1;
            try {
                while((len = reader.read(c))!=-1) {
                    String s = new String(c);
                    System.out.println(s);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(reader!=null)
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

}