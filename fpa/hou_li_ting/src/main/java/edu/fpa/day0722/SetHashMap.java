package edu.fpa.day0722;

public class SetHashMap {
    Node2[] table;


    public SetHashMap(){
        table = new Node2[16];
    }

    public void put(Object key,Object value) {
        int hash = Myhash(key.hashCode(),table.length);
        Node2 newNode = new Node2();
        newNode.hash = hash;
        newNode.key = key;
        newNode.value = value;
        newNode.next = null;

        Node2 node1 = table[hash];
        boolean keyreapt =false;

        if(node1==null) {
            table[hash] = newNode;
        }else {
            while(node1!=null) {
                Object k = node1.key;
                if(k.equals(key)) {
                    keyreapt = true;
                    node1.value = value;
                    break;
                }else {
                    if(node1.next!=null)
                        node1 = node1.next;
                    else break;
                }
                if(!keyreapt) {
                    node1.next = newNode;
                }
            }
        }

    }
    public static int Myhash(int index,int length) {
        return index&(length-1);
    }
    public static void main(String[] args) {
        SetHashMap s = new SetHashMap();
        s.put(10,"aa");
        s.put(20, "bb");
        s.put(30, "cc");
        s.put(20, "bbdd");
        s.put(74, "ee");
        s.put(90, "ff");
        System.out.println(s);

    }
}
