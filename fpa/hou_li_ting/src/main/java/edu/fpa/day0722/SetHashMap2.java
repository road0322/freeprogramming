package edu.fpa.day0722;

public class SetHashMap2 {
    Node2[] table;


    public SetHashMap2(){
        table = new Node2[16];
    }

    public void put(Object key,Object value) {
        Node2 newNode = new Node2();
        newNode.hash = Myhash(key.hashCode(),table.length);
        newNode.key = key;
        newNode.value = value;
        newNode.next = null;

        Node2 node1 = table[newNode.hash];
        boolean keyreapt =false;
        Node2 node2 = null;
        if(node1==null) {
            table[newNode.hash] = newNode;
        }else {
            while(node1!=null) {
                if(node1.key.equals(key)) {
                    keyreapt = true;
                    node1.value = value;
                    break;
                }else {
                    if(node1!=null)
                        node2 = node1;
                    node1 = node1.next;

                }
                if(!keyreapt) {
                    node2.next = newNode;
                }
            }
        }

    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{") ;
        for(int i=0;i<table.length;i++) {
            Node2 node = table[i];
            while(node!=null) {
                sb.append(node.key+":"+node.value+",");
                node = node.next;
            }
        }
        sb.setCharAt(sb.length()-1, '}');
        return sb.toString();
    }
    public static int Myhash(int index,int length) {
        return index&(length-1);
    }
    public static void main(String[] args) {
        SetHashMap2 s = new SetHashMap2();
        s.put(10,"aa");
        s.put(20, "bb");
        s.put(30, "cc");
        s.put(20, "bbdd");
        s.put(53, "ee");
        s.put(85, "ff");
        System.out.println(s);

    }
}
