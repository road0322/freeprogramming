package edu.fpa.day0801;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DataTest02 {
    public static void main(String[] args) throws IOException {
        OutputStream o = new FileOutputStream("hello.txt");
        DataOutputStream dos = new DataOutputStream(o);
        dos.writeUTF("zzzz");
        dos.writeInt(18);
        dos.writeBoolean(true);
        dos.writeChar('a');
        dos.flush();
        DataInputStream dis = new DataInputStream(new FileInputStream("hello.txt"));
        String s = dis.readUTF();
        int i = dis.readInt();
        boolean bool = dis.readBoolean();
        char c = dis.readChar();
        System.out.println(s);
        System.out.println(i);
        System.out.println(bool);
        System.out.println(c);
    }
}