package edu.fpa.day0801;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DataTest {
    public static void main(String[] args) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeUTF("zzzz");
        dos.writeInt(18);
        dos.writeBoolean(true);
        dos.writeChar('a');
        dos.flush();
        byte[] b = baos.toByteArray();
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(b));
        String s = dis.readUTF();
        int i = dis.readInt();
        boolean bool = dis.readBoolean();
        char c = dis.readChar();
        System.out.println(s);
        System.out.println(i);
        System.out.println(bool);
        System.out.println(c);
    }
}
