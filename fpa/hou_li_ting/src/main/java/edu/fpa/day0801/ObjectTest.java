package edu.fpa.day0801;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream dos = new ObjectOutputStream(baos);
        dos.writeUTF("zzzz");
        dos.writeInt(18);
        dos.writeBoolean(true);
        dos.writeChar('a');
        dos.writeObject("aaaaaaaa");
        Employee e = new Employee("张三",5000);
        dos.writeObject(e);
        dos.flush();
        byte[] b = baos.toByteArray();
        ObjectInputStream dis = new ObjectInputStream(new ByteArrayInputStream(b));
        String s = dis.readUTF();
        int i = dis.readInt();
        boolean bool = dis.readBoolean();
        char c = dis.readChar();
        Object s1 = dis.readObject();
        Object e1 = dis.readObject();
        System.out.println(s);
        System.out.println(i);
        System.out.println(bool);
        System.out.println(c);
        if(s1 instanceof String) {
            String sObj = (String)s;
            System.out.println(sObj);
            if(e1 instanceof Employee) {
                Employee e2 = (Employee)e1;
                System.out.println(e2.getName());
            }
        }
    }
}
class Employee implements java.io.Serializable{
    private String name ;
    private double salary;
    public Employee(String name, double salary) {
        super();
        this.name = name;
        this.salary = salary;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }

}