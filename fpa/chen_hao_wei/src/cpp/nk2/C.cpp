#include <bits/stdc++.h>
using namespace std;
int main(){
    int t,x[2],y[2];
    char s[100];
    bool flag;
    // freopen("input.in","r",stdin);
    // freopen("output.out","w",stdout);
    cin>>t;
    while(t--){
        flag=true;
        scanf("%s",s+1);
        x[0]=x[1]=y[0]=y[1]=0;
        for(int i=1;i<10;i++){
            if(s[i]=='1')x[i%2]++;
            else if(s[i]=='?')y[i%2]++;
            if(x[1]+y[1]>x[0]+(10-i)/2+i%2||x[0]+y[0]>x[1]+(10-i)/2){
                cout<<i<<endl;
                flag=false;
                break;
            }
        }
        if(flag)
        cout<<10<<endl;
    }
    return 0;
}