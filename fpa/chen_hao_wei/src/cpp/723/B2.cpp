#include <bits/stdc++.h>
using namespace std;
int main(){
    int t;
    int n,k;
    long long res;
    // freopen("input.in","r",stdin);
    // freopen("output.out","w",stdout);
    cin>>t;
    while(t--){
        cin>>n>>k;
        res=0;
        vector<int> v(n);
        vector<int> a(n+1);
        for(int i=0;i<n;i++){
            scanf("%d",&v[i]);
            if(a[v[i]]<k) a[v[i]]++;
        }
        for(int i=1;i<=n;i++){
            res+=a[i];
        }
        res/=k;
        vector<int> b(k+1,res);
        vector<set<int> > vs(n+1);
        for(int i=0;i<n;i++){
            if(a[v[i]]){
                a[v[i]]--;
                bool flag=true;
                for(int j=1;j<=k;j++){
                    if(vs[v[i]].count(j)==0&&b[j]){
                        vs[v[i]].insert(j);
                        cout<<j<<' ';
                        b[j]--;
                        flag=false;
                        break;
                    }
                }
                if(flag)cout<<0<<' ';
            }else cout<<0<<' ';
        }
        cout<<endl;
    }
    return 0;
}