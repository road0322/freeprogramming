package edu.fpa.day0729;

public class DaemonTest {
    public static void main(String[] args) {
        God g = new God();
        You y = new You();
        Thread t = new Thread(g);
        t.setDaemon(true);  //将用户线程调整为守护线程
        t.start();
//		new Thread(g).start();
        new Thread(y).start();
    }
}
class You implements Runnable{
    @Override
    public void run() {
        for(int i=1;i<5;i++) {
            System.out.println("happy life");
        }
        System.out.println("hhhhhhhh");
    }
}
class God implements Runnable{
    @Override
    public void run() {
        for(int i=1;i<10;i++) {
            System.out.println("bless you");
        }
    }
}
