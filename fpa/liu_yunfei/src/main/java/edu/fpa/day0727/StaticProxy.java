package edu.fpa.day0727;

public class StaticProxy {
    public static void main(String[] args) {
        new WeddingCompary(new You()).happyMarry();
        //new Thread(线程对象).start();
    }
}
interface Marry{
    void happyMarry();
}
//真实角色
class You implements Marry{
    @Override
    public void happyMarry() {
        System.out.println("you and 嫦娥终于奔月了");
    }
}
//代理角色
class WeddingCompary implements Marry{
    //真实角色
    private Marry targe;
    public WeddingCompary(Marry targe) {
        this.targe = targe;
    }
    @Override
    public void happyMarry() {
        ready();
        this.targe.happyMarry();
        after();
    }

    private void ready() {
        System.out.println("布置蜡烛");
    }

    private void after() {
        System.out.println("闹洞房");
    }
}
