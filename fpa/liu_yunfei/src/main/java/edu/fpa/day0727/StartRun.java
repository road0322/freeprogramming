package edu.fpa.day0727;

public class StartRun implements Runnable{
    /**
     * 线程入口点
     */
    public void run() {
        for(int i=0;i<3;i++) {
            System.out.println("一边听歌");
        }
    }
    public static void main(String[] args) {
        //创建实现类对象
        StartRun sr = new StartRun();
        //创建代理类对象
        Thread t = new Thread(sr);
        //启动
        t.start();  //不保证立即运行，由cpu调用

        /*new Thread(new StartRun()).start();*/ //该方法适用于出现一次，即单继承

//		st.run();    普通调用，则先循环上方for，在进行下面的for循环

        for(int i=0;i<3;i++) {
            System.out.println("一边写作业");
        }
    }

}
