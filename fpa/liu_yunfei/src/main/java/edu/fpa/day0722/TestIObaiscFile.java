package edu.fpa.day0722;

import java.io.*;

public class TestIObaiscFile {
    public static void main(String[] args) {
        //1、创建源
        File src = new File("a.txt");
        //2、选择流
        InputStream is = null;
        try {
            is = new FileInputStream(src);
            //3、操作(读取)
            int temp;
            while((temp=is.read())!=-1) {
                System.out.println((char)temp);
            }
            //4、释放资源
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(null!=is) {
                    is.close();  //避免出现空指针异常
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
