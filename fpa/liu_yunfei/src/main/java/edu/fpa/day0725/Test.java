package edu.fpa.day0725;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Test {
    public static void main(String[] args) {
        try(BufferedReader reader=
                    new BufferedReader(
                            new InputStreamReader(
                                    new URL("http://www.baidu.com").openStream(),"UTF-8"));){
            //3、操作(读取)
            String msg;
            while((msg=reader.readLine())!=null) {
                System.out.print(msg);
            }
        }catch(IOException e){
            System.out.println("操作异常");
        }
    }
}
