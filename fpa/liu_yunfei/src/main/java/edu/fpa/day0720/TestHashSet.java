package edu.fpa.day0720;

import java.util.HashMap;

public class TestHashSet {
    HashMap map;
    private static final Object PRESENT = new Object();
    public TestHashSet() {
        map = new HashMap();
    }
    public int size() {
        return map.size();
    }
    public void add(Object o) {
        map.put(o,PRESENT);
    }

    @Override
    public String toString() {
        StringBuilder num = new StringBuilder();
        num.append("[");

        for(Object key:map.keySet()) {
            num.append(key+",");
        }
        num.append("]");
        return num.toString();
    }
    public static void main(String[] args) {
        TestHashSet set = new TestHashSet();
        set.add("ccc");
        set.add("aaa");
        set.add("bbb");

        System.out.println(set);
    }
}
