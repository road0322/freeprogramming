package edu.fpa.day0721;

/**
 * 测试表格数据的存储
 * 体会ORM思想
 * 每一行数据使用javabean对象存储，多行使用反到map或list中
 * @author lyf
 *
 */
import java.util.*;

public class TestStoreData {
    public static void main(String[] args) {
        User user1 = new User(997, "刘云飞", 20000, "2020-3-4");
        User user2 = new User(597, "刘小飞", 13000, "2021-5-4");
        User user3 = new User(957, "刘鱼飞", 40000, "2014-4-23");
        List<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        for(User u:list) {
            System.out.println(u);
        }

        Map<Integer,User> num = new HashMap<>();
        num.put(1, user1);
        num.put(2, user2);
        num.put(3, user3);
        Set<Integer> keyset = num.keySet();
        for (Integer key : keyset) {
            System.out.println(key+"---"+num.get(key));
        }
    }
}

class User{
    private int id;
    private String name;
    private double salary;
    private String hiredata;


    //一个javabean必须要有get和set方法，以及无参构造器!!!!
    public User() {

    }

    public User(int id, String name, double salary, String hiredata) {
        super();
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.hiredata = hiredata;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getHiredata() {
        return hiredata;
    }

    public void setHiredata(String hiredata) {
        this.hiredata = hiredata;
    }

    @Override
    public String toString() {
        return "id:"+id+" name:"+name+" salary:"+salary+" hiredata:"+hiredata;
    }
}
