package edu.fpa.day0804;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class Receive_TMultiClient3 {
    private DataInputStream dis;
    private Socket client;
    private boolean isRunning;
    public Receive_TMultiClient3(Socket client) {
        this.client = client;
        try {
            dis = new DataInputStream(client.getInputStream());
        } catch (IOException e) {
            System.out.println("---error---");
            release();
        }
    }
    //接收消息
    private String receive() {
        String msg = "";
        try {
            msg = dis.readUTF();
        } catch (IOException e) {
            System.out.println("-----receive error-----");
            release();
        }
        return msg;
    }

    public void run() {
        while(isRunning) {
            String msg = receive();
            if(!msg.equals("")) {
                System.out.println(msg);
            }
        }
    }

    //释放资源
    private void release() {
        this.isRunning = false;
        Utils_TMultiChat3.close(dis,client);
    }
}
