package edu.fpa.day0804;

import java.io.Closeable;

public class Utils_TMultiChat3 {
    /**
     * 释放资源
     */
    public static void close(Closeable... targets) {
        for(Closeable target:targets) {
            try {
                if(null!=target) {
                    target.close();
                }
            }catch(Exception e) {

            }
        }
    }
}
