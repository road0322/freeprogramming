package edu.fpa.day0803;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class UdpClient {
    public static void main(String[] args) throws Exception {
        System.out.println("发送中");
        //1、使用DatagramSocket 指点端口 创建发送端
        DatagramSocket client = new DatagramSocket(4444);
        //2、准备数据 （一定转成字节数组）
        String data = "刘云飞";
        byte[] datas = data.getBytes();
        //3、封装成DatagramPacket包裹，需要指定目的地
        DatagramPacket packet = new DatagramPacket(datas,0,data.length(),
                new InetSocketAddress("localhost",6666));
        //4、发送包裹send（DatagramPacket p）
        client.send(packet);
        //5、释放资源
        client.close();
    }
}
