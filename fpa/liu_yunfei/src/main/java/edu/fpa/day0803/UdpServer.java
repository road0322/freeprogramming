package edu.fpa.day0803;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UdpServer {
    public static void main(String[] args) throws IOException {
        System.out.println("接收中");
        //1、使用DatagramSocket 指点端口 创建接收端
        DatagramSocket server = new DatagramSocket(6666);
        //2、准备容器 封装成DatagramPacket包裹
        byte[] container = new byte[1024*60];
        DatagramPacket packet = new DatagramPacket(container,0,container.length);
        //3、阻塞式接收包裹receive（DatagramPacket p）
        server.receive(packet); //阻塞式
        //4、分析数据：byte[] get Data()
        //				   get Length()
        byte[] datas = packet.getData();
        int len = packet.getLength(); //长度可要可不要
        System.out.println(new String(datas,0,len));

        //5、释放资源
        server.close();
    }
}
