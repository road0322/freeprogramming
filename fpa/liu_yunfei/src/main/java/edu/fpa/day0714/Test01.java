package edu.fpa.day0714;

import java.util.Arrays;
public class Test01 {
    public static void main(String[] args) {
        int[] arr  = {10,20,6,8,5,21,50,53,};
        Arrays.sort(arr);

        System.out.println(Arrays.toString(arr));
        System.out.println(myBinarySearch(arr,20));

    }
    public  static  int  myBinarySearch(int[] arr,int value) {
        int low = 0;
        int high = arr.length-1;

        while(low<=high){
            int mid = (low+high)/2;

            if(value==arr[mid]) {
                return mid;
            }
            if(value > arr[mid]) {
                low = mid+1;
            }
            if(value < arr[mid]) {
                high = mid-1;
            }

        }
        return -1;
    }
}
