package edu.fpa.day0716;
/**
 * 自定义ArrayList并增加泛型，积累经验
 */

import java.util.ArrayList;

public class Test03<E> {
    public static void main(String[] args) {
        ArrayList s1 = new ArrayList(20);
        s1.add("aa");
        s1.add("bb");

        System.out.println(s1);
    }
    private Object[] elementData;
    private int size;

    private static final int DEFALT_CAPACITY = 10;

    public Test03() {
        elementData = new Object[DEFALT_CAPACITY];
    }

    public Test03(int capacity) {
        elementData = new Object[capacity];
    }

    public void add(E element) {
        elementData[size++] = element;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();
        //[a,b,c]
        sb.append("[");
        for(int i=0;i<size;i++) {
            sb.append(elementData[i]+",");
        }
        sb.setCharAt(sb.length()-1,']');
        return super.toString();
    }
}
