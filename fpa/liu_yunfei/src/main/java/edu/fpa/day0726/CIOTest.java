package edu.fpa.day0726;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;

public class CIOTest {
    public static void main(String[] args) {
        //文件大小
        long len = FileUtils.sizeOf(new File("src/com/lyf/commons/CIOTest01.java"));
        System.out.println(len);
        //目录大小
        len = FileUtils.sizeOf(new File("D:/workspace/IO_study04"));
        System.out.println(len);
    }

}