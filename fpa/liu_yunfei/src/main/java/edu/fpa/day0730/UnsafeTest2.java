package edu.fpa.day0730;

public class UnsafeTest2 {
    public static void main(String[] args) {
        //账户
        Account account = new Account(100,"结婚礼金");
        Drawing you = new Drawing(account,80,"你");
        Drawing wife = new Drawing(account,70,"对象");
        you.start();
        wife.start();
    }
}
//账户
class Account{
    int money;   //金钱
    String name; //名字
    public Account(int money, String name) {
        super();
        this.money = money;
        this.name = name;
    }

}
//模拟取款
class Drawing extends Thread{
    Account account;  //取钱账户
    int darwingMoney; //取钱数额
    int packetTotal; //口袋总额

    public Drawing(Account account, int darwingMoney, String name) {
        super(name);
        this.account = account;
        this.darwingMoney = darwingMoney;
    }

    @Override
    public void run() {
        if(account.money-darwingMoney<0) {
            return;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        account.money -= darwingMoney;
        packetTotal += darwingMoney;
        System.out.println(this.getName()+"-->账户余额为："+account.money);
        System.out.println(this.getName()+"-->口袋的钱为："+packetTotal);
    }
}
