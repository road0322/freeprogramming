package edu.fpa.day0730;

import java.util.ArrayList;
import java.util.List;

public class UnsafeTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for(int i=0;i<10;i++) {
            new Thread(()->{
                list.add(Thread.currentThread().getName());
            }).start();
        }
        System.out.println(list.size());
    }
}
