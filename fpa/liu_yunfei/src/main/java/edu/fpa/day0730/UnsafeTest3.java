package edu.fpa.day0730;

import java.util.ArrayList;
import java.util.List;

public class UnsafeTest3 {
    public static void main(String[] args) {
        //一份资源
        UnsafeWeb12306 wb = new UnsafeWeb12306();
        //多个代理
        new Thread(wb,"刘云飞").start();
        new Thread(wb,"刘小飞").start();
        new Thread(wb,"刘鱼飞").start();
    }
}

class UnsafeWeb12306 implements Runnable {
    //票数
    private int ticketnums = 10;
    private boolean flag = true;
    @Override
    public void run() {
        while(flag) {
            test();
        }
    }
    //线程安全，同步
    public void test() {
        if(ticketnums<0) {
            flag = false;
            return;
        }
        //模拟延时
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"-->"+ticketnums--);
    }
}
