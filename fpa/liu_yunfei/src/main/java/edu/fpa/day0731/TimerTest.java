package edu.fpa.day0731;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {
    public static void main(String[] args) {
        Timer timer = new Timer();
        //执行安排
        timer.schedule(new MyTask(), 1000); //执行一次
        //执行多次  timer,schedule(new MyTask(),1000,2000)  不停止，一直执行
    }
}
//任务类
class MyTask extends TimerTask {
    public void run() {
        for(int i=0;i<4;i++) {
            System.out.println("放空大脑,休息休息");
        }
        //System.out.println("——————end————");
    }

}
