package edu.java.day0731;

/**
 * 线程安全：在并发时保证数据的准确性、效率尽可能高
 * synchronized
 *1.同步方法
 * 2、同步块
 */
public class SynTest02 {
    public static void main(String[] args) {
        //账户
        edu.java.day0730.Account account=new edu.java.day0730.Account(100,"结婚礼金");
        SafeDrawing you =new SafeDrawing(account,80,"可悲的你");
        SafeDrawing wife=new SafeDrawing(account,90,"happy的她");
        you.start();
        wife.start();
    }
}
//模拟取钱
class SafeDrawing extends  Thread{
    edu.java.day0730.Account account;//取钱账户
    int drawingMoney;// 取钱的次数
    int packetTotal;//口袋的总数
    public SafeDrawing(edu.java.day0730.Account account, int drawingMoney, String name){
        super(name);
        this.account=account;
        this.drawingMoney=drawingMoney;
    }
    @Override
    public void run() {
        test();
    }
    //目标不对 锁定失败 应该锁定account
    public synchronized void test(){
        if(account.money-drawingMoney<0){
            return;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        account.money-=drawingMoney;
        packetTotal+=drawingMoney;
        System.out.println(this.getName()+"-->账户余额为:"+account.money);
        System.out.println(this.getName()+"-->口袋余额为:"+packetTotal);
    }
}
