package edu.java.day0731;

/**
 * 线程安全：在并发时保证数据的准确性、效率尽可能高
 * synchronized
 *1.同步方法
 * 2、同步块，目标跟明确
 */
public class SynBlockTest01 {

        public static void main(String[] args) {
            //账户
            edu.java.day0730.Account account=new edu.java.day0730.Account(100,"结婚礼金");
            SynDrawing you =new SynDrawing(account,80,"可悲的你");
            SynDrawing wife=new SynDrawing(account,90,"happy的她");
            you.start();
            wife.start();
        }
    }
    //模拟取钱  线程安全
    class SynDrawing extends  Thread {
        edu.java.day0730.Account account;//取钱账户
        int drawingMoney;// 取钱的次数
        int packetTotal;//口袋的总数

        public SynDrawing(edu.java.day0730.Account account, int drawingMoney, String name) {
            super(name);
            this.account = account;
            this.drawingMoney = drawingMoney;
        }

        @Override
        public void run() {
            test();
        }

        //目标account
        public void test() {
            //提高性能
            if(account.money<=0){
                return;
            }
            //同步块
            synchronized (account) {
                if (account.money - drawingMoney < 0) {
                    return;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.money -= drawingMoney;
                packetTotal += drawingMoney;
                System.out.println(this.getName() + "-->账户余额为:" + account.money);
                System.out.println(this.getName() + "-->口袋余额为:" + packetTotal);
            }
        }
    }
