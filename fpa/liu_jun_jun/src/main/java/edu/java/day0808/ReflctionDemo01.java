package edu.java.day0808;

/**
 * 测试各种类型(class,interface,enum,annotation,primitive type,void)Java.lang.Class对象的获取方式
 *
 */
public class ReflctionDemo01 {
    public static void main(String[] args) {
        String path="edu.java.day0808.test.User";

        try {

            Class<?> clazz=Class.forName(path);
            //对象是用来表示一些数据。一个类被加载后，JVM会创建一个对应该类的Class对象，类的整个结构信息会被放到对应的Class对象中。
            //这个Class对象就像是一面镜子一样，通过这面镜子我们可以看到对应类的全部信息。

            System.out.println(clazz.hashCode());//一个类只对应一个Class对象

            Class strClazz=String.class;
            Class strClazz2=path.getClass();
            System.out.println(strClazz==strClazz2);

            Class intClazz=int.class;

            int[] a=new int[10];
            int[] a2=new int[30];

            System.out.println();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
