package edu.java.day0808;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 使用反射读取注解的信息，模拟处理注解信息的流程
 */
public class Demo03 {
    public static void main(String[] args) {
        try {
            Class clazz=Class.forName("edu.java.day0808.Student");
            //获得类的所有有效注解
            Annotation[] anootations=clazz.getAnnotations();
            for (Annotation a:anootations
                 ) {
                System.out.println(a);
            }
            //获得类的指定注解
            schoolTable st=(schoolTable) clazz.getAnnotation(schoolTable.class);
            System.out.println(st.value());
            //获得类的属性的注解
            Field f=clazz.getDeclaredField("name");
            SxtField sxtField=f.getAnnotation(SxtField.class);
            System.out.println(sxtField.columnName()+"--"+sxtField.type()+"--"+sxtField.length());

            //根据获得的表名、字段的信息，拼出DDL语句，然后使用JDBC执行这个SQL，在数据库中生成相关的表
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
