package edu.java.day0729;
/*
lambda 推导+参数
 */
public class LambdaTest04 {

    public static void main(String[] args) {
        new Thread(()->{
            System.out.println("一边学习lambda");
        }).start();

        new Thread(()-> System.out.println("一边学习奔溃")).start();
    }
}
