package edu.java.day0729;

import edu.java.day0728.Racer;

/**
 * sleep模拟休息
 */
public class BlockedSleep02 {

}
    class Racer1 implements Runnable{
        private  String winner;//胜利者
        @Override
        public void run() {
            for(int steps=1;steps<=100;steps++){
                //模拟休息
                if(Thread.currentThread().getName().equals("zyz")&&steps%10==0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName()+"-->"+steps);
                //比赛是否结束
                boolean flag=gameOver(steps);
                if(flag){
                    break;
                }
            }
        }
        private boolean gameOver(int steps){
            if(winner!=null) {//存在胜利者
                return true;
            }else{
                if(steps==100){
                    winner=Thread.currentThread().getName();
                    System.out.println("winner==>"+winner);
                    return true;
                }
            }
            return false;
        }

        public static void main(String[] args) {
            edu.java.day0728.Racer racer=new edu.java.day0728.Racer();
            new Thread(racer,"zyz").start();
            new Thread(racer,"ysm").start();
        }
    }


