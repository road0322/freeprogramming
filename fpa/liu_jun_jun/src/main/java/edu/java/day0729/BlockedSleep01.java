package edu.java.day0729;

/**
 * sleep模拟网络延时，放大了发生问题的可能性
 */
public class BlockedSleep01 {

    public static void main(String[] args) {
        //一份资源
        edu.java.day0728.Web12306 web=new edu.java.day0728.Web12306();
        //多个代理
        new Thread(web,"zyz").start();
        new Thread(web,"ysm").start();
        new Thread(web,"llk").start();
    }
}
class Web12306 implements Runnable{
    //票数
    private int ticketNums=99;
    @Override
    public void run() {
        while(true){
            if(ticketNums<0){
                break;
            }
            //模拟网络延时
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"-->"+ticketNums--);
        }
    }
}