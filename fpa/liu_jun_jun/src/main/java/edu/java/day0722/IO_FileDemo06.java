package edu.java.day0722;

import java.io.File;
import java.io.IOException;

/*
其他信息
creatNewFile()      不存在才创建，存在创建成功
delete()            删除已经存在的文件
 */
public class IO_FileDemo06 {
    public static void main(String[] args) throws IOException {
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722/IO.txt`");
        boolean flag=src.createNewFile();
        System.out.println(flag);
        flag=src.delete();
        System.out.println(flag);
        System.out.println("-----------");

        //不是文件夹
        src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0723");
        flag=src.createNewFile();
        System.out.println(flag);


        flag=src.delete();
        System.out.println(flag);

        //补充: con com3...操作系统的设备名，不能正确创建
        src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722/con");
        src.createNewFile();
    }
}
