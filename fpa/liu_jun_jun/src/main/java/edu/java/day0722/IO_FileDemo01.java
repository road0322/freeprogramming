package edu.java.day0722;

import java.io.File;

/*


 */
public class IO_FileDemo01 {


    /*
        构建File对象
         */
    public static void main(String[] args) {
        String path="D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722/IO_FileDemo01.java";
    //构建File对象
        File src=new File(path);
        System.out.println(src.length());
    //2.构建File对象
        src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722","IO_FileDemo01.java");
        System.out.println(src.length());
    //3.构建File对象
        src=new File(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722"),"IO_FileDemo01.java");
        System.out.println(src.length());
    }
}
