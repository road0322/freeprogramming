package edu.java.day0722;

import java.io.File;

/*
列出下一级
1.list()：列出下级名称
2.listFiles():列出下级File对象

列出所有的盘符listRoots()
 */
public class DirDemo02 {
    public static void main(String[] args) {
        File dir=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722");
        //列出下级名称
        String[] subsNames=dir.list();
        for(String s:subsNames){
            System.out.println(s);
        }
        //下级对象 listFile()
        File[] subFiles=dir.listFiles();
        for(File s:subFiles){
            System.out.println(s.getAbsolutePath());
        }

        //所有盘符
        File[] roots=dir.listRoots();
        for(File r:roots){
            System.out.println(r.getAbsolutePath());
        }
    }
}
