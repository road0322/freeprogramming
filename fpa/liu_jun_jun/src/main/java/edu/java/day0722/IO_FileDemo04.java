package edu.java.day0722;

import java.io.File;

/*
    文件状态
    1.不存在：exits
    2.存在：
        文件：isFile
        文件夹：isDiretory
     */
public class IO_FileDemo04 {

    public static void main(String[] args) {
        File src=new File("liu_jun_jun/src/main/java/edu/java/day0722/IO_FileDemo04.java");
        System.out.println(src.getAbsolutePath());
        System.out.println("是否存在："+src.exists());
        System.out.println("是否是文件"+src.isFile());
        System.out.println("是否是文件夹"+src.isDirectory());


        src=new File("IO_FileDemo04.java");
        System.out.println("-------------------");
        System.out.println("是否存在："+src.exists());
        System.out.println("是否是文件"+src.isFile());
        System.out.println("是否是文件夹"+src.isDirectory());

        src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun");
        System.out.println("-------------------");
        System.out.println("是否存在："+src.exists());
        System.out.println("是否是文件"+src.isFile());
        System.out.println("是否是文件夹"+src.isDirectory());


        src=new File("XXX");
        if(null==src||!src.exists()){
            System.out.println("文件不存在");
        }else{
            if(src.isFile()){
                System.out.println("文件操作");
            }else{
                System.out.println("文件夹操作");
            }
        }
    }

}
