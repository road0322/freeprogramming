package edu.java.day0722;

import java.io.File;

public class IO_FileDemo02 {
    /*
    构建File对象
    相对路径与绝对路径
    1)存在盘符：绝对路径
    2）不存在盘符：相对路径    ,当前目录user.dir
     */
    public static void main(String[] args) {
        String path="D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722/IO_FileDemo01.java";
        //绝对路径
        File src=new File(path);
        System.out.println(src.getAbsolutePath());
        //相对路径
        //System.out.println(System.getProperties());
        src=new File("IO_FileDemo.java");
        System.out.println(src.getAbsolutePath());
        //构建一个不存在的文件
        src=new File("aaa/IO2.java");
        System.out.println(src.getAbsolutePath());
    }
}
