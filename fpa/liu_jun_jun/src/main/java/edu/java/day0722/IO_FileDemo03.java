package edu.java.day0722;

import java.io.File;

/*
名称或路径
getName();          名称
getPath();          相对、绝对
getAbsolutePath();  绝对
getPath();          上路径 不存在 返回null
 */
public class IO_FileDemo03 {
    public static void main(String[] args) {
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/IO_FileDemo03.java");
        //基本信息
        System.out.println("名称"+src.getName());
        System.out.println("路径"+src.getPath());
        System.out.println("绝对路径"+src.getAbsolutePath());
        System.out.println("父路径"+src.getParent());
        System.out.println("父对象"+src.getParentFile().getName());
    }
}
