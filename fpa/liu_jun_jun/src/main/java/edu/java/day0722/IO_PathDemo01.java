package edu.java.day0722;

import java.io.File;

//
public class IO_PathDemo01 {
    public static void main(String[] args) {
        /*
        *   \ /名称分隔符  separator
        *
         */
        String path="D:\\study\\后端学习\\freeprogramming\\fpa\\liu_jun_jun\\src\\main\\java\\edu\\java\\day0722";
        System.out.println(File.separatorChar);
        //建议
        //1.\
        path="D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722";
        System.out.println(path);
        //2.常量拼接
        path ="D:"+File.separator+"study"+File.separator+"后端学习"+File.separator+"freeprogramming"+File.separator+"fpa"+File.separator+"liu_jun_jun"+File.separator+"src"+File.separator+"main"+File.separator+"java"+File.separator+"edu"+File.separator+"java"+File.separator+"day0722";

        System.out.println(path);
    }
}
