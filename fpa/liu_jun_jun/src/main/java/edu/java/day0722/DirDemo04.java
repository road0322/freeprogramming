package edu.java.day0722;

import java.io.File;

/*
打印子孙级目录和文件名称
 */
public class DirDemo04 {
    public static void main(String[] args) {
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722");
        printName(src,0);
    }
    //打印子孙级目录和文件名称
    public static void printName(File src,int deep){
        //控制前面层次感
        for(int i=0;i<deep;i++){
            System.out.print("-");
        }
        //打印名称
        System.out.println(src.getName());
        if(null==src||!src.exists()){//递归头
            return;
        }else if(src.isDirectory()){//目录
            for(File s:src.listFiles()){
                printName(s,deep+1);//递归体
            }
        }
    }
}
