package edu.java.day0722;

import java.io.File;

/*
使用面向对象：统计文件夹大小
 */
public class DirCount {
    //大小
    private long len;
    //文件夹路径
    private  String path;
    //文件个数
    private int fileSize;
    //文件夹个数
    private int dirSize;

    public int getFileSize() {
        return fileSize;
    }

    public int getDirSize() {
        return dirSize;
    }

    //源
    private  File src;
    public DirCount(String path){
        this.path=path;
        this.src=new File(path);
        count(this.src);
    }
    public static void main(String[] args) {
        DirCount dir=new DirCount("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722");
        System.out.println(dir.getLen()+"--->"+dir.getFileSize()+"--->"+dir.getDirSize());


    }

    public void setLen(long len) {
        this.len = len;
    }

    public long getLen() {
        return len;
    }

    //统计大小
    private void count(File src){
        //获取大小
        if(null!=src&&src.exists()){
            if(src.isFile()) {//大小
                this.fileSize++;
                len+=src.length();
            }else{//子孙级
                this.dirSize++;
                for(File s:src.listFiles()){
                    count(s);
                }
            }
        }
    }

}
