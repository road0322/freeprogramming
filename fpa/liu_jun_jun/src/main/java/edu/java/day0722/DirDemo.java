package edu.java.day0722;

import java.io.File;

/*
统计文件夹大小
 */
public class DirDemo {
    public static void main(String[] args) {
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0722");
        count(src);
        System.out.println(len);
    }
    private  static long len=0;
    public static void count(File src){
        //获取大小
        if(null!=src&&src.exists()){
            if(src.isFile()) {//大小
                len+=src.length();
            }else{//子孙级
                for(File s:src.listFiles()){
                    count(s);
                }
            }
        }
    }
}
