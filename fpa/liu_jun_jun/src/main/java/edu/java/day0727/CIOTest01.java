package edu.java.day0727;

import org.apache.commons.io.FileUtils;

import java.io.File;

/*
大小
 */
public class CIOTest01 {
    public static void main(String[] args) {
        //文件大小
        long len= FileUtils.sizeOf(new File("src/main/java/edu/java/day0727/CIOTest01.java"));
        System.out.println(len);
        //目录大小
        len=FileUtils.sizeOf(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727"));
        System.out.println(len);
    }
}
