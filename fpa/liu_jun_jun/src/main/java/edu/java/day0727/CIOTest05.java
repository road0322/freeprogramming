package edu.java.day0727;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/*
拷贝
 */
public class CIOTest05 {
    public static void main(String[] args) throws IOException {
        //复制文件
        //FileUtils.copyFile(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/L.jpg"),new File("p-copy.jpg"));
        //复制文件到目录
        //FileUtils.copyFileToDirectory(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/L.jpg"),new File("lib"));
        //复制目录到目录页
        //FileUtils.copyDirectoryToDirectory(new File("lib"),new File("lib2"));
        //复制目录
        //FileUtils.copyDirectory(new File("lib"),new File("lib2"));
        //拷贝URL内容
//        String url="https://pic2.zhimg.com/v2-7d01cab20858648cbf62333a7988e6d0_qhd.jpg";
//        FileUtils.copyURLToFile(new URL(url),new File("marvel.jpg"));
        String datas= IOUtils.toString(new URL("http://www.baidu.com"),"UTF-8");
        System.out.println(datas);
    }
}
