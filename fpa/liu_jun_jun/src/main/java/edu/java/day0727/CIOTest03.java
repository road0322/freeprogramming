package edu.java.day0727;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*
读取内容
 */
public class CIOTest03 {
    public static void main(String[] args) throws IOException {
        //读取文件
        String msg= FileUtils.readFileToString(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727/emp.txt"),"UTF-8");
        System.out.println(msg);
        byte[] datas=FileUtils.readFileToByteArray(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727/emp.txt"));
        System.out.println(datas.length);
        //逐行读取
        List<String> msgs=FileUtils.readLines(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727/emp.txt"),"UTF-8");
        for(String string:msgs){
            System.out.println(string);
        }
        LineIterator it=FileUtils.lineIterator(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727/emp.txt"),"UTF-8");
        while(it.hasNext()){
            System.out.println(it.nextLine());
        }
    }
}
