package edu.java.day0727;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.EmptyFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;

import java.io.File;
import java.util.Collection;

/*
列出子孙级
 */
public class CIOTest02 {
    public static void main(String[] args) {
        Collection<File> files=FileUtils.listFiles(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727"), EmptyFileFilter.NOT_EMPTY,null);
        for(File file:files){
            System.out.println(file.getAbsolutePath());
        }
        System.out.println("---------------------");
        files=FileUtils.listFiles(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727"),
                EmptyFileFilter.NOT_EMPTY, DirectoryFileFilter.INSTANCE);
        for(File file:files){
            System.out.println(file.getAbsolutePath());
        }
        System.out.println("---------------------");
        files=FileUtils.listFiles(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727"),
                FileFilterUtils.or(new SuffixFileFilter("class"),new SuffixFileFilter("java")), DirectoryFileFilter.INSTANCE);
        for(File file:files){
            System.out.println(file.getAbsolutePath());
        }
        System.out.println("---------------------");
        files=FileUtils.listFiles(new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/main/java/edu/java/day0727"),
                FileFilterUtils.and(new SuffixFileFilter("class"),EmptyFileFilter.NOT_EMPTY), DirectoryFileFilter.INSTANCE);
        for(File file:files){
            System.out.println(file.getAbsolutePath());
        }
    }
}
