package edu.java.day0727;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/*
写出内容
 */
public class CIOTest04 {
    public static void main(String[] args) throws IOException {
        //写出文件
        FileUtils.write(new File("happy.sxt"),"zyz是狗\r\n","UTF-8");
        FileUtils.writeStringToFile(new File("happy.sxt"),"sbzyz\r\n","UTF-8",true);
        FileUtils.writeByteArrayToFile(new File("happy.sxt"),"zzzyz\r\n".getBytes("UTF-8"),true);

        //写出列表
        List<String> datas=new ArrayList<String>();
        datas.add("zyz");
        datas.add("llk");
        datas.add("ysm");
        FileUtils.writeLines(new File("happy.sxt"),datas,"........",true);
    }
}
