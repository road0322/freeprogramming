package edu.java.day0724;

import java.io.*;

/*
打印流PrintWriter
 */
public class PrintTest02 {
    public static void main(String[] args) throws FileNotFoundException {
        //打印流System.out
        PrintWriter pw= new PrintWriter(
                new BufferedOutputStream(
                        new FileOutputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/print.txt",true)//自动提供刷新功能
                ));
        pw.println("打印流");
        pw.println(true);
        //ps.flush();
        pw.close();
    }
}
