package edu.java.day0724;

import java.io.*;

/*
打印流PrintStream
 */
public class PrintTest {
    public static void main(String[] args) throws FileNotFoundException {
        //打印流System.out
        PrintStream ps=System.out;



        ps=new PrintStream(
                new BufferedOutputStream(
                        new FileOutputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/print.txt",true)//自动提供刷新功能
                ));
        ps.println("打印流");
        ps.println(true);
        //ps.flush();

        ps.close();
        //重定向输出端
        System.setOut(ps);
        System.out.println("change");
        //重定向回控制台
        System.setOut(new PrintStream(
                new BufferedOutputStream(
                        new FileOutputStream(FileDescriptor.out))));
        System.out.println("i am backing...");
    }
}
