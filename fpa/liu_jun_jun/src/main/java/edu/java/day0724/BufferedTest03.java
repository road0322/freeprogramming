package edu.java.day0724;

import java.io.*;

/*
文件字符输入流             加入缓冲流
第一个程序：理解操作步骤    标准
1、创建流
2、选择流
3、操作
4、释放资源
 */
public class BufferedTest03 {
    public static void main(String[] args) {
        //1、创建源
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt");
        //2、选择流
        BufferedReader reader=null;
        try {
            reader=new BufferedReader(new FileReader(src));
            //3、操作（分段读取）
            String line=null;
            while((line=reader.readLine())!=null){
                //字符数组-->字符串
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //4、释放资源
            try {
                if(null!=reader) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

