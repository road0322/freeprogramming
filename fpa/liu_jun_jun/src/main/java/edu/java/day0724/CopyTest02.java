package edu.java.day0724;
/*
文件的拷贝
思考：利用递归 制作文件夹的拷贝
 */
import java.io.*;

public class CopyTest02 {
    public static void main(String[] args) {
        copy("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt","D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/dest.txt");
    }

    public static  void copy(String srcPath,String destPath){
        //1.创建源
        File src=new File(srcPath);//源头
        File dest=new File(destPath);//目的地
        //2.选择流
        try{
            BufferedReader br=new BufferedReader(new FileReader(src));
            BufferedWriter bw=new BufferedWriter(new FileWriter(dest));
            //3、操作（逐行读取）
            String line=null;
            while((line=br.readLine())!=null){
                bw.write(line);
                bw.newLine();
            }
            bw.flush();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
