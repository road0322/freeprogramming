package edu.java.day0724;


import java.io.*;
import java.util.Date;

/*
对象流
1.写出后读取
2.读取的顺序与写出的顺序保持一致
3.不是所有对象都可以序列化Serializable

ObjectOutputStream
ObjectInputStream
 */
public class ObjectTest02 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //写出-->序列化
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        ObjectOutputStream dos=new ObjectOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/Obj.ser")
                ));
        //操作数据类型 +数据
        dos.writeUTF("编码辛酸泪");
        dos.writeInt(18);
        dos.writeBoolean(false);
        dos.writeChar('a');
        //对象
        dos.writeObject("谁解其中味");
        dos.writeObject(new Date());
        Employee emp=new Employee("马云",400);
        dos.writeObject(emp);
        dos.flush();
        dos.close();
        //读取-->反序列化
        ObjectInputStream ois=new ObjectInputStream(
                new BufferedInputStream(
                        new FileInputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/Obj.ser")));
        //顺序与写出一致
        String msg=ois.readUTF();
        int age=ois.readInt();
        boolean flag=ois.readBoolean();
        char ch=ois.readChar();
        System.out.println(flag);
        //对象的数据还原
        Object str=ois.readObject();
        Object date=ois.readObject();
        Object employee=ois.readObject();


        if(str instanceof String){
            String strObj=(String)str;
            System.out.println(strObj);
        }
        if(date instanceof Date){
            Date DateObj=(Date)date;
            System.out.println(DateObj);
        }
        if(employee instanceof Employee){
            Employee empObj=(Employee) employee;
            System.out.println(empObj.getName()+"-->"+empObj.getSalary());
        }
        ois.close();
    }
}