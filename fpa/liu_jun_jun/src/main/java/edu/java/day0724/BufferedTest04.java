package edu.java.day0724;

import java.io.*;
import java.nio.charset.StandardCharsets;

/*
文件字符输出流     加入缓存流
1.创建源
2.选择流
3.操作（写出内容）
4.释放资源
 */
public class BufferedTest04 {
    public static void main(String[] args) {
        //1.创建源
        File dest=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/dest.txt");
        //2.选择流
        BufferedWriter writer=null;
        try{
            writer=new BufferedWriter(new FileWriter(dest));
            //3.操作（写出）
            //写法一
//            String msg="IO is so easy\r\n张宇哲是狗";
//            char[] datas=msg.toCharArray();//字符串-->字符数组
//            writer.write(datas,0, datas.length);

            //写法二
//            String msg="IO is so easy\r\n张宇哲是狗";
//            writer.write(msg);
//            writer.write("add");


            //写法三
            writer.append("IO is so easy");
            writer.newLine();
            writer.append("张宇哲是狗");
            writer.flush();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            //4.释放资源
            if(null!=writer){
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
