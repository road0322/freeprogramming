package edu.java.day0720;



import java.util.Arrays;
import java.util.Scanner;

public class test1 {
    public static void main(String[] args) {
        Scanner id=new Scanner(System.in);
        long s=id.nextLong();
        long e=id.nextLong();
        long k=id.nextLong();
        int a=(int)(e-s+1);
        long[] n=new long[a];
        for(int i=0;i<n.length;i++){
            n[i]=s;
            s++;
        }
        minPrime((n));
        System.out.println(minnum(n,k));
    }
    public static void minPrime(long[] n){
        for(int i=0;i<n.length;i++){
            if(n[i]%2==0){
                n[i]=2;
            }else {
                for(int j=3;j<n[i];j++){
                    if(n[i]%j==0) {
                        if (isPrime(j)) {
                            n[i] = j;
                            break;
                        }
                    }
                }
            }
        }
    }

    public static boolean isPrime(int i) {
        for (long j = 2; j < Math.sqrt(i) ; j++) {
            if(i%j==0){
                return false;
            }
        }
        return true;
    }
    public static long minnum(long[] n,long k){
        Arrays.sort(n);
        long sum=0;
        for(int i=0;i<k;i++){
            sum=sum+n[i];
        }
        return sum;
    }
}

