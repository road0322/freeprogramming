package edu.java.day0723;

import java.io.*;
import java.nio.charset.StandardCharsets;

/*
1.图片 读取到字节数组中
2.字节数组写出到文件

获取数据: toByteArray()
 */
public class IO_Test09 {
    public static void main(String[] args) {
        //图片转成字节数组
        byte[] datas=fileToByteArray("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/p.jpg");
        System.out.println(datas.length);
        byteArrayToFile(datas,"D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/p-byte.jpg");
    }
    //图片到字节数组中
    //1)图片到程序   FileInputStream
    //2)程序到字节数组ByteArrayOutputStream
    public static byte[] fileToByteArray(String filePath){
        //1、创建源与目的地
        File src=new File(filePath);
        byte[] dest=null;
        //2、选择流
        InputStream is=null;
        ByteArrayOutputStream baos=null;
        try {
            is=new FileInputStream(src);
            baos=new ByteArrayOutputStream();
            //3、操作（分段读取）
            byte[] flush=new byte[1024*10];//缓冲容器
            int len=-1;//接受长度
            while((len=is.read(flush))!=-1){
                baos.write(flush,0, len);       //写出到字节数组中

            }
            baos.flush();
            return baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //4、释放资源
            try {
                if(null!=is) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    //字节数组写出到图片
    //1)字节数组到程序     ByteArrayInputStream
    //2)程序到文件           FileOutputSream
    public  static void byteArrayToFile(byte[] src,String filePath){
        //1.创建源
        File dest=new File(filePath);
        //2、选择流
        InputStream is=null;
        OutputStream os=null;
        try {
            is=new ByteArrayInputStream(src);
            os=new FileOutputStream(dest);
            //3、操作（分段读取）
            byte[] flush=new byte[5];//缓冲容器
            int len=-1;//接受长度
            while((len=is.read(flush))!=-1){
                os.write(flush,0, len);

            }
            os.flush();
        }  catch (IOException e) {
            e.printStackTrace();
        }finally {
            //4、释放资源
            if(null!=os){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
