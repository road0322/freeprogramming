package edu.java.day0723;

import java.io.*;

/*
1.封装拷贝
2.封装释放
 */
public class IO_FileUtils {
    public static void main(String[] args) {
       //文件到文件
        try {
            InputStream is=new FileInputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt");
            OutputStream os=new FileOutputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc-copy.txt");
            copy(is,os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //文件到字节数组
        byte[] datas=null;
        try {
            InputStream is=new FileInputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/p.jpg");
            ByteArrayOutputStream os=new ByteArrayOutputStream();
            copy(is,os);
            datas=os.toByteArray();
            System.out.println(datas.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //字节数组到文件
        try {
            InputStream is=new ByteArrayInputStream(datas);
            OutputStream os=new FileOutputStream("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/p-copy.jpg");
            copy(is,os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
    对接输入输出流
     */
    public static  void copy(InputStream is,OutputStream os){
        try{
            //3、操作（分段读取）
            byte[] flush=new byte[1024];//缓冲容器
            int len=-1;//接受长度
            while((len=is.read(flush))!=-1){
                os.write(flush,0, len);
            }
            os.flush();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            close(is,os);

        }
    }
    /*
    4.释放资源 分别关闭 先打开的后关闭
     */
    public static void close(InputStream is,OutputStream os){

        if(null!=os){
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            if(null!=is) {
                is.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //释放资源
    public static void close(Closeable... ios){
        for(Closeable io:ios){
            try {
                if(null!=io) {
                    io.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

