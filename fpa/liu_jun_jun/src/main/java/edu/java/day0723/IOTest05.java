package edu.java.day0723;

import java.io.*;

/*
文件字符输入流
第一个程序：理解操作步骤    标准
1、创建流
2、选择流
3、操作
4、释放资源
 */
class IO_Test05 {
    public static void main(String[] args) {
        //1、创建源
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt");
        //2、选择流
        Reader reader=null;
        try {
            reader=new FileReader(src);
            //3、操作（分段读取）
            char[] flush=new char[1024];//缓冲容器
            int len=-1;//接受长度
            while((len=reader.read(flush))!=-1){
                //字符数组-->字符串
                String str=new String(flush,0,len);
                System.out.println(str);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //4、释放资源
            try {
                if(null!=reader) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

