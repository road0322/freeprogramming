package edu.java.day0723;
/*
文件的拷贝
思考：利用递归 制作文件夹的拷贝
 */
import java.io.*;

public class IO_Copy {
    public static void main(String[] args) {
        copy("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt","D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/dest.txt");
    }

    public static  void copy(String srcPath,String destPath){
        //1.创建源
        File src=new File(srcPath);//源头
        File dest=new File(destPath);//目的地
        //2.选择流
        InputStream is=null;
        OutputStream os=null;
        try{
            is=new FileInputStream(src);
            os=new FileOutputStream(dest);
            //3、操作（分段读取）
            byte[] flush=new byte[1024];//缓冲容器
            int len=-1;//接受长度
            while((len=is.read(flush))!=-1){
                os.write(flush,0, len);
            }
            os.flush();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            //4.释放资源 分别关闭 先打开的后关闭
            if(null!=os){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                if(null!=is) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
