package edu.java.day0723;

import java.io.*;
import java.nio.charset.StandardCharsets;

/*
文件拷贝：文件字节输入、输出流
1.创建源
2.选择流
3.操作（写出内容）
4.释放资源
 */
public class IO_CopyFile {
    public static void main(String[] args) {
        //1.创建源
        File src=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt");//源头
        File dest=new File("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/dest.txt");//目的地
        //2.选择流
        InputStream is=null;
        OutputStream os=null;
        try{
            is=new FileInputStream(src);
            os=new FileOutputStream(dest);
            //3、操作（分段读取）
            byte[] flush=new byte[1024];//缓冲容器
            int len=-1;//接受长度
            while((len=is.read(flush))!=-1){
                os.write(flush,0, len);
            }
            os.flush();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            //4.释放资源 分别关闭 先打开的后关闭
            if(null!=os){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                if(null!=is) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
