package edu.java.day0719;

import jdk.nashorn.internal.objects.annotations.Constructor;

import java.util.*;

//测试表格数据
//体会EOM思想
//每一行数据使用一个javabean对香港，整个表格使用Map/List
public class StoreDatatest2 {
    public static void main(String[] args) {
        User user1=new User(1001,"zyz",200000,"2020.5.5");
        User user2=new User(1002,"llk",70000,"2002.5.5");
        User user3=new User(1003,"zzw",50000,"2014.5.5");

        List<User> list=new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        for(User u:list){
            System.out.println(u);
        }
        Map<Integer,User> m=new HashMap<>();
        Set<Integer> keyset=m.keySet();
        m.put(1001,user1);
        m.put(1002,user2);
        m.put(1003,user3);

        for(Integer key:keyset){
            System.out.println(key+"===="+m.get(key));
        }

    }
}
class User{
    private int id;
    private String name;
    private int salary;
    private String hiredate;

    //一个完整的javabean，要有set、get方法 ，以及无参数构造器
    public User(){

    }
    public User(int id, String name, int salary, String hiredate) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.hiredate = hiredate;
    }
    public void setId(int id) {
        this.id = id;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public String getHiredate() {
        return hiredate;
    }
    @Override
    public String toString(){
        return "id:"+id+",name:"+name+",salary:"+salary+",hiredate"+hiredate;
    }

}
