package edu.java.day0730;

//线程不安全：数据有负数、相同

public class UnsafeTest01 {
    public static void main(String[] args) {
        //一份资源
        UnsafeWeb12306 web = new UnsafeWeb12306();
        //多个代理
        new Thread(web, "zyz").start();
        new Thread(web, "ysm").start();
        new Thread(web, "llk").start();
    }

    static class UnsafeWeb12306 implements Runnable {
        //票数
        private int ticketNums = 10;
        private boolean flag = true;

        public void run() {
            while (flag) {
                test();
            }
        }

        public void test() {
            if (ticketNums < 0) {
                flag = false;
                return;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "-->" + ticketNums--);
        }


    }
}