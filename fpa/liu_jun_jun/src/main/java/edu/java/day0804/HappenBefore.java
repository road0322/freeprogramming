package edu.java.day0804;

/**
 * 指令重排：代码执行顺序与预期不一致
 * 目的提高性能
 */
public class HappenBefore {
    //变量1
    private static int a=0;
    //变量2
    private static boolean flag=false;

    public static void main(String[] args) throws InterruptedException {
        //线程1 更改数据
        Thread t=new Thread(()->{
                    a=1;
                    flag=true;
                });
        //线程2 读取数据
        Thread t2=new Thread(()->{
            if(flag){
                a*=1;
            }
            if(a==0){
                System.out.println("happen before a->"+a);
            }
        });

        t.start();
        t2.start();
        //合并线程 线程的插队
        t.join();
        t2.join();
    }
}
