package edu.java.day0804;

/**
 * 不可重入锁：锁不可以延续使用
 */
public class LockTest01 {
    Lock lock=new Lock();
    public void a() throws InterruptedException {
        lock.lock();
        doSometh();
        lock.unlock();
    }
    //不可重入
    public void doSometh() throws InterruptedException {
        lock.lock();
        //.............
        lock.unlock();
    }
    public static void main(String[] args) throws InterruptedException {
        LockTest01 test=new LockTest01();
        test.a();
        test.doSometh();
    }
}
class Lock{
    //是否占用
    private boolean isLocked=false;
    //使用锁
    public synchronized void lock() throws InterruptedException {
        while(isLocked){
            wait();
        }

        isLocked=true;
    }
    //释放锁
    public void unlock(){
        isLocked=false;
        notify();
    }
}
