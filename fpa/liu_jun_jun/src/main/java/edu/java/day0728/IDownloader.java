package edu.java.day0728;

public class IDownloader implements Runnable{
    private String url;//远程路径
    private String name;//存储名字

    public IDownloader(String url,String name){
        this.url=url;
        this.name=name;
    }
    @Override
    public void run() {
        WebDownloader wd=new WebDownloader();
        wd.download(url,name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        IDownloader td1=new IDownloader("","");
        IDownloader td2=new IDownloader("","");
        IDownloader td3=new IDownloader("","");

        //启动三个线程
        new Thread(td1).start();
        new Thread(td2).start();
        new Thread(td3).start();
    }
}
