package edu.java.day0728;

public class TDownloader extends Thread{
    private String url;//远程路径
    private String name;//存储名字

    public TDownloader(String url,String name){
        this.url=url;
        this.name=name;
    }
    @Override
    public void run() {
        WebDownloader wd=new WebDownloader();
        wd.download(url,name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        TDownloader td1=new TDownloader("","");
        TDownloader td2=new TDownloader("","");
        TDownloader td3=new TDownloader("","");

        //启动三个线程
        td1.start();
        td2.start();
        td3.start();
    }
}
