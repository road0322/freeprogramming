package edu.java.day0728;

import java.util.concurrent.*;
/*
了解创建线程的方式三
 */
public class CDownloader implements Callable<Boolean> {
    private String url;//远程路径
    private String name;//存储名字

    public CDownloader(String url,String name){
        this.url=url;
        this.name=name;
    }
    @Override
    public Boolean call() throws Exception {
        return null;
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        CDownloader cd1=new CDownloader("","");
        CDownloader cd2=new CDownloader("","");
        CDownloader cd3=new CDownloader("","");
        //创建执行服务
        ExecutorService ser= Executors.newFixedThreadPool(3);
        //提交执行：
        Future<Boolean> result1=ser.submit(cd1);
        Future<Boolean> result2=ser.submit(cd2);
        Future<Boolean> result3=ser.submit(cd3);
        //获取结果
        boolean r1=result1.get();
        boolean r2=result2.get();
        boolean r3=result3.get();
        System.out.println(r3);
        //关闭服务
        ser.shutdownNow();

    }
}
