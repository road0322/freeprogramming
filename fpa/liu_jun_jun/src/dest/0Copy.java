package edu.java.day0724;
/*
文件的拷贝
思考：利用递归 制作文件夹的拷贝
 */
import java.io.*;

public class copy {
    public static void main(String[] args) {
        long t1=System.currentTimeMillis();
        copy("D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/abc.txt","D:/study/后端学习/freeprogramming/fpa/liu_jun_jun/src/dest.txt");
        long t2=System.currentTimeMillis();
        System.out.println(t2-t1);
    }

    public static  void copy(String srcPath,String destPath){
        //1.创建源
        File src=new File(srcPath);//源头
        File dest=new File(destPath);//目的地
        //2.选择流
        try{
            InputStream is=new BufferedInputStream(new FileInputStream(src));
            OutputStream os=new BufferedOutputStream(new FileOutputStream(dest));
            //3、操作（分段读取）
            byte[] flush=new byte[1024];//缓冲容器
            int len=-1;//接受长度
            while((len=is.re