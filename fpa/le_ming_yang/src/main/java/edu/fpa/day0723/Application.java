package edu.fpa.day0723;

public class Application {
    public static void main(String[] args) {
        //类，抽象的， 实例化
        //类实例化后会返回一个自己的对象！
        //student对象就是一个Student类的具体实例！
        Student xiaoming = new Student();
        Student xiaohong = new Student();

        xiaoming.name = "小明";
        xiaoming.age =3;
        xiaoming.xuehao = 01;

        System.out.println(xiaoming.name);
        System.out.println(xiaoming.age);
        System.out.println(xiaoming.xuehao);

        xiaohong.name = "小红";
        xiaohong.age = 3;
        xiaohong.xuehao = 02;

        System.out.println(xiaohong.name);
        System.out.println(xiaohong.age);
        System.out.println(xiaohong.xuehao);



    }
}
