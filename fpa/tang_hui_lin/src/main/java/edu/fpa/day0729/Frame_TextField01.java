package src.main.java.edu.fpa.day0729;

import java.awt.*;
import java.awt.event.*;
public class Frame_TextField01 {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        new MyFrame();
    }

}
class MyFrame extends Frame{
    public MyFrame() {
        TextField textField=new TextField();
        add(textField);
        MyActionListener myActionListener=new MyActionListener();
        textField.addActionListener(myActionListener);
        textField.setEchoChar('*');
        setVisible(true);
        pack();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

}

class MyActionListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        TextField field=(TextField) e.getSource();
        field.setText("");
    }

}
