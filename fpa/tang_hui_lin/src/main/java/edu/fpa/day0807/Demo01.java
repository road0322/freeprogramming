package src.main.java.edu.fpa.day0807;

public class Demo01 {

    static final int mod = 1921;

    public static void main(String[] args) {
        System.out.println(pow(7, 2020));
    }

    static int pow(int a, int n) {
        if (n == 0) return 1;
        if (n == 1) return a;
        if (0 == (n & 1)) return pow((a % mod) * (a % mod), n / 2) % mod;
        return ((a % mod) * (pow((a % mod) * (a % mod), n / 2) % mod)) % mod;
    }
    /*  7 月 1 日是建党日，从 1921 年到 2020 年，中国共产党已经带领中国人民
        走过了 99 年。
     请计算：7 2020 7^{2020}7
         2020
    mod 1921，其中 A mod B 表示 A 除以 B 的余数。*/
}
