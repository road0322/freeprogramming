package src.main.java.edu.fpa.day0807;

import java.io.*;

public class Demo02 {

    public static void main(String[] args) throws IOException {
        InputReader in = new InputReader(System.in, 200);
        int[] str = new int[30], table = new int[128];
        for (int i = 0; i < 30; i++)
            str[i] = in.nextChar();
        for (int i = 0; i < 52; i++) {
            int v = in.nextChar(), w = in.nextChar();
            table[w] = v;
        }
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < 30; i++)
            out.appendCodePoint(table[str[i]]);
        System.out.print(out);
    }

    static class InputReader {

        InputStream in;
        int next, len;
        byte[] buff;

        InputReader(InputStream in) { this(in, 8192); }

        InputReader(InputStream in, int buffSize) {
            this.buff = new byte[buffSize];
            this.next = this.len = 0;
            this.in = in;
        }

        int getByte() {
            if (next >= len)
                try {
                    next = 0;
                    len = in.read(buff);
                    if (len == -1) return -1;
                } catch (IOException e) { }
            return buff[next++];
        }

        int nextChar() {
            int c = getByte();
            while (c <= 32 || c == 127) c =getByte();
            return c;
        }
    }

}
