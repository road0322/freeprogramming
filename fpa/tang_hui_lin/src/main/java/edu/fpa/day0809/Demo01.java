package src.main.java.edu.fpa.day0809;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class Demo01 {

    public static void main(String[] args) throws IOException {
        PrintWriter out = new PrintWriter(System.out);
        long n = nextLong(System.in);
        do {
            out.print(n);
            out.write(' ');
        } while ((n /= 2) > 0);
        out.close();
    }

    static long nextLong(InputStream in) throws IOException {
        long n = 0;
        int c = in.read();
        while (c < '0' || c > '9') c = in.read();
        while (c >='0' && c <='9') {
            n = n * 10 + c - '0';
            c = in.read();
        }
        return n;
    }
    /*时间限制: 1.0s 内存限制: 512.0MB 本题总分：20 分

      问题描述
      有一个序列，序列的第一个数是 n，后面的每个数是前一个数整除 2，请输出这个序列中值为正数的项。

      输入格式
      输入一行包含一个整数 n。

      输出格式
      输出一行，包含多个整数，相邻的整数之间用一个空格分隔，表示答案。*/
}
