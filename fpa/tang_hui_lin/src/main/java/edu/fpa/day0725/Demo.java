package src.main.java.edu.fpa.day0725;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo {
    public static void main(String[] args) {
        // 创建分割文件的存储文件夹
        String basefile = "d:data/";
        new File(basefile).mkdir();
        // 要分割的文件
        String filename = "d:readme.txt";
        // 文件的后缀
        String suf = filename.substring(filename.lastIndexOf("."));
        // 读取文件的长度
        long length = (new File(filename)).length();
        // 为了代码的通用性，可以指定每个文件分割的大小，从而确定分割成几个文件
        // 设置分割文件的大小
        int sonfile = 10;
        // 分割文件的数量
        int times = (int) Math.ceil(length / (float) sonfile);
        // 创建线程池，根据分割文件的数量创建线程
        ExecutorService threadPool = Executors.newFixedThreadPool(times);
        for (int i = 0; i < times; i++) {
            final int j = i;
            // 设置随机的文件名
            String newfilename = UUID.randomUUID().toString();
            threadPool.execute(() -> {
                try (// 使用randomAccessFile来读取文件
                     RandomAccessFile randomAccessFile = new RandomAccessFile(filename, "rw");
                     // 使用FileOutputStream写入文件
                     FileOutputStream fileOutputStream = new FileOutputStream(basefile + newfilename + suf);) {
                    int pos = sonfile * j;
                    randomAccessFile.seek(pos);
                    byte[] buf = new byte[sonfile];
                    int size = randomAccessFile.read(buf);
                    fileOutputStream.write(buf, 0, size);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        System.out.println("文件分割完成");
        threadPool.shutdown();
    }

}