package src.main.java.edu.fpa.day0806;

import java.util.Scanner;
public class Demo02 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(sum(n));
    }
    public static double sum(int n) {
        double m=0;
        if(n%2==0) {
            for(int i=2;i<=n;i+=2) {
                m+=(double)1/i;
            }
        }else {
            for(int i=1;i<=n;i+=2) {
                m+=(double)1/i;
            }
        }
        return m;
    }
}

/*  编写一个函数，输入n为偶数时，调用函数求1/2+1/4+…+1/n,当输入n为奇数时，调用函数1/1+1/3+…+1/n(利用指针函数)
    分析：编写一个方法，根据输入数据的奇偶性执行不同的操作
    需要注意的点：求和时需要用double类型*/