package src.main.java.edu.fpa.day0806;


import java.util.Scanner;

public class Demo01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        boolean[] arr = new boolean[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = true;
        }
        int Count = n;
        int index = 0;
        int countNum = 0;
        while (Count > 1) {
            if (arr[index] == true) {
                countNum++;
                if (countNum == 3) {
                    arr[index] = false;
                    Count--;
                    countNum = 0;
                }
            }
            index++;
            if (index == n) {
                index = 0;
            }
        }
        for (int i = 0; i < n; i++) {
            if (arr[i] == true) {
                System.out.println(i + 1);
            }
        }
    }
}
/*  有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，问最后留下的是原来第几号的那位。
    分析：定义一个布尔类型的数组然后用while循环选择出符合要求的数
    需要注意的点：需要注意若是人数变量循环到末尾需使人数变量归零。*/


