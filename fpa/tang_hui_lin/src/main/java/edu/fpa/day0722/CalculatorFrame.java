package edu.fpa.day0722;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowListener;

import javax.swing.*;

public class CalculatorFrame{
    public static WindowsOperation win;
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        win=new WindowsOperation("简易计算器");
    }
}



class WindowsOperation extends JFrame implements ItemListener,ActionListener{
    JTextField inputNumberOne,inputNumberTwo;  //定义两个输入组件
    JComboBox<String> choicebox;  //定义符号的复选框
    JButton button;   //确定按钮
    JTextArea area;   //显示结果的文本区
    String fuhao;
    JMenuBar menubar;  //菜单条
    JMenu menu; //菜单
    JMenuItem item; //菜单项



    WindowsOperation(String s){
        init(s);
        setVisible(true);  //设置为可见状态
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //设置关闭窗口的状态
    }
    public  void init(String s) {
        setTitle(s);   //设置标题
        setBounds(606,330,320,285);
        setLayout(new FlowLayout());//设置布局类型
        setResizable(false);
        inputNumberOne=new JTextField(5);
        inputNumberTwo=new JTextField(5);
        button=new JButton("计算");
        choicebox=new JComboBox<String>();
        choicebox.addItem("选择运算符号");
        String[] arr= {"+","-","*","/","%","lg","ln","^"}; //运算符号
        for(int i=0;i<arr.length;i++) {
            choicebox.addItem(arr[i]); //将符号添加到下拉列表中
        }
        area=new JTextArea(9,30);


        choicebox.addItemListener(this);//注册ItemListener监视器
        button.addActionListener(this);//注册ActionListener监视器

        /*    添加菜单条     */
        menubar=new JMenuBar();
        menu=new JMenu("菜单");
        item=new JMenuItem("退出");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
        menu.add(item);
        item.addActionListener(this);
        menubar.add(menu);
        setJMenuBar(menubar);

        add(inputNumberOne);
        add(choicebox);
        add(inputNumberTwo);
        add(button);
        add(area);
        add(new JLabel("THL"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button) {
            // TODO Auto-generated method stub
            double number1,number2;
            double result=0; //总结果
            number2=Double.parseDouble(inputNumberTwo.getText());//获取第二个输入框的值
            if(fuhao.equals("lg")) {
                result=Math.log10(number2);
                area.setText(null);
                area.append(fuhao+number2+" = "+result+"\n");
            }else if(fuhao.equals("ln")){
                result=Math.log(number2);
                area.setText(null);
                area.append(fuhao+number2+" = "+result+"\n");
            }else {
                try {
                    number1=Double.parseDouble(inputNumberOne.getText());//获取第一个输入框的值

                    if(fuhao.equals("+")) {
                        result=number1+number2;
                    }else if(fuhao.equals("-")){
                        result=number1-number2;
                    }else if(fuhao.equals("*")) {
                        result=number1*number2;
                    }else if(fuhao.equals("/")) {
                        if(number2==0) {
                            throw new Exception("The dividend is 0");
                        }
                        result=number1/number2;
                    }else if(fuhao.equals("^")) {
                        result=Math.pow(number1,number2);
                    }
                    area.setText(null);
                    area.append(number1+" "+fuhao+" "+number2+" = "+result+"\n");
                }catch(Exception x) {
                    String str="请正确输入数据";
                    JOptionPane.showMessageDialog(this,str,"错误提示",JOptionPane.WARNING_MESSAGE);
                    inputNumberOne.setText(null);
                    inputNumberTwo.setText(null);
                }
            }
        }else if(e.getSource()==item) {
            this.dispose();
        }

    }
    @Override
    public void itemStateChanged(ItemEvent e) { //复选框事件
        // TODO Auto-generated method stub
        fuhao=choicebox.getSelectedItem().toString();//将选取的符号传到String fuhao中
    }

}
