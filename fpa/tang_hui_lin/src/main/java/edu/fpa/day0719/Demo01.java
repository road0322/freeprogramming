package edu.fpa.day0719;

import javax.swing.*;
        import java.awt.*;

public class Demo01 extends JFrame {

    public Demo01(){

        this.setVisible(true);
        this.setBounds(100,100,348,148);
        this.setDefaultCloseOperation(3);
        this.setTitle("时间设置");

        //一个面板
        JPanel p = new JPanel();
        p.setBackground(Color.yellow);

        //第一行
        JLabel l1 = new JLabel("当前时间：");
        JLabel l2 = new JLabel("时");
        JLabel l3 = new JLabel("分");
        JLabel l4 = new JLabel("秒");
        TextField t1 = new TextField(5);
        TextField t2 = new TextField(5);
        TextField t3 = new TextField(5);

        //第二行
        JLabel l5 = new JLabel("闹钟时间：");
        JLabel l6 = new JLabel("时");
        JLabel l7 = new JLabel("分");
        JLabel l8 = new JLabel("秒");
        TextField t4 = new TextField(5);
        TextField t5 = new TextField(5);
        TextField t6 = new TextField(5);

        //第三行&按钮
        JLabel l = new JLabel("闹钟设置");
        JButton b1 = new JButton("开");
        JButton b2 = new JButton("关");

        //添加第一行
        p.add(l1);
        p.add(t1);
        p.add(l2);
        p.add(t2);
        p.add(l3);
        p.add(t3);
        p.add(l4);

        //添加第二行
        p.add(l5);
        p.add(t4);
        p.add(l6);
        p.add(t5);
        p.add(l7);
        p.add(t6);
        p.add(l8);

        //添加第三行
        p.add(l);
        p.add(b1);
        p.add(b2);
        this.add(p);

    }

    public static void main(String[] args){

        new Demo01();

    }


}