package com.guo.hh;

public class He {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "He{" +
                "name='" + name + '\'' +
                '}';
    }
}
