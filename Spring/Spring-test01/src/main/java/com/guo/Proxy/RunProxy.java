package com.guo.Proxy;


//注意:这个动态需要使用java自带的反射来完成
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

class RProxy implements InvocationHandler {
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), target.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        look();
        Object a=method.invoke(target,objects);
        return a;
    }
    public void look(){
        System.out.println("中介带人看房");
    }
}

public class RunProxy {
    public static void main(String[] args) {
        Host host = new Host();
        RProxy rProxy = new RProxy();
        rProxy.setTarget(host);
        Rent target = (Rent) rProxy.getProxy();
        target.rent();
    }
}
