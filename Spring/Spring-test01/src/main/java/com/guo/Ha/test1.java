package com.guo.Ha;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
public class test1 {
    private  String name;
    @Value("GUOJIA")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "test1{" +
                "name='" + name + '\'' +
                '}';
    }
}
