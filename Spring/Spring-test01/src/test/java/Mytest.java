import com.guo.Hello.Hello;
import com.guo.Hello.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mytest {
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
        Hello a= (Hello) context.getBean("hello");
        Hello b= (Hello) context.getBean("hello2");
        Hello c= (Hello) context.getBean("hello3");
        Person d= (Person) context.getBean("person4");
        Person e= (Person) context.getBean("person5");
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());
        System.out.println(d.toString());
        System.out.println(e.toString());
    }
}
